package org.destinosustentavel.dsmovel.tools

import android.graphics.drawable.Drawable
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestBuilder
import org.destinosustentavel.dsmovel.*
import org.destinosustentavel.dsmovel.databinding.AdDetailsBinding
import org.destinosustentavel.dsmovel.model.Ad
import org.destinosustentavel.dsmovel.model.User
import java.text.SimpleDateFormat

class AdapterAd(
    private val ads: ArrayList<Ad>,
    private val users: ArrayList<User?>,
    private val imgs: ArrayList<RequestBuilder<Drawable>?>,
    private var viewType: Int
) : RecyclerView.Adapter<AdapterAd.ViewHolder>() {

    var onbtRightClick: ((Int, Ad, User?) -> Unit)? = null
    var onbtLeftClick: ((Int, Ad, User?) -> Unit)? = null
    var onbtCenterClick: ((Int, Ad, User?) -> Unit)? = null
    var onbtProfileClick: ((User?) -> Unit)? = null

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val binding: AdDetailsBinding?
        init {
            binding = DataBindingUtil.bind(itemView)
            binding!!.btRight.setOnClickListener{
                this@AdapterAd.ads?.get(adapterPosition)?.let { ad ->
                    onbtRightClick?.invoke(adapterPosition, ad, users.get(adapterPosition))
                }
            }
            binding.btCenter.setOnClickListener{
                this@AdapterAd.ads?.get(adapterPosition)?.let { ad ->
                    onbtCenterClick?.invoke(adapterPosition, ad, users.get(adapterPosition))
                }
            }
            binding.btLeft.setOnClickListener{
                this@AdapterAd.ads?.get(adapterPosition)?.let { ad ->
                    onbtLeftClick?.invoke(adapterPosition, ad, users.get(adapterPosition))
                }
            }
            binding.profileName.setOnClickListener{
                this@AdapterAd.ads?.get(adapterPosition)?.let { ad ->
                    onbtProfileClick?.invoke(users.get(adapterPosition))
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.details_ad, parent, false))
    }

    override fun getItemCount(): Int {
        return this.ads.count()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ad = ads.get(position)
        val user = users.get(position)
        val img = imgs.get(position)

        setData(holder.binding!!,ad)
        if(!ad.status.equals(AVALIABLE)){
            user?.let{
                img?.let {it2 ->
                    setUser(holder.binding!!, it, it2)
                }
            }
        }
        when(viewType){
            AD_FEED ->{
                user?.let{
                    img?.let {it2 ->
                        setUser(holder.binding!!, it, it2)
                    }
                }
                feed(holder.binding!!)
            }
            AD_PROFILE ->{
                user?.let{
                    img?.let {it2 ->
                        setUser(holder.binding!!, it, it2)
                    }
                }
                profile(holder.binding!!)
            }
            AD_DONE ->{
                user?.let{
                    img?.let {it2 ->
                        if(!ad.status.equals(AVALIABLE)){
                            setUser(holder.binding!!, it, it2)
                        }
                    }
                }
                done(holder.binding!!, ad)
            }
            AD_ANSWER ->{
                user?.let{
                    img?.let {it2 ->
                        setUser(holder.binding!!, it, it2)
                    }
                }
                answer(holder.binding!!, ad)
            }
        }
        if(ad.status == DELETED){
            deleted(holder.binding)
        }
    }



    private fun deleted(binding: AdDetailsBinding?) {
        binding!!.profileName.setText("Este anúncio não está mais disponível")
        binding.title.visibility = View.GONE
        binding.materialTypeImage.visibility = View.GONE
        binding.materialTypeText.visibility = View.GONE
        binding.amount.visibility = View.GONE
        binding.sale.visibility = View.GONE
        binding.progress.visibility = View.GONE
        binding.btRight.visibility = View.GONE
        binding.btCenter.visibility = View.GONE
        binding.btLeft.visibility = View.GONE
        binding.createdAt.visibility = View.GONE
        binding.end.visibility = View.GONE
        binding.description.visibility = View.GONE
    }

    private fun setData(binding: AdDetailsBinding, ad: Ad){
        binding!!.title.text = ad.title
        binding.materialTypeText.text = ad.type + " de " + ad.material_type
        binding.amount.text = ad.amount.toString() + " " + ad.unity
        binding.createdAt.text =
            "Ativo desde " + SimpleDateFormat("dd/MM/yyyy").format(ad.created_at?.time) +
                    " ultima edição em " + SimpleDateFormat("dd/MM/yyyy").format(ad.edited_at?.time)
        binding.end.text = "Valido até " + SimpleDateFormat("dd/MM/yyyy").format(ad.end?.time)
        binding.description.text = ad.description

        binding.materialTypeImage.setImageResource(
            when (ad.material_type) {
                "Plastico" -> R.drawable.ic_plastic_recycler
                "Metal" -> R.drawable.ic_metal_recycler
                "Vidro" -> R.drawable.ic_glass_recycler
                "Papel" -> R.drawable.ic_paper_recycler
                "Eletrônico" -> R.drawable.ic_ewaste_recycler
                else -> R.drawable.ic_organic_recycler
            }
        )

        if(ad.sell) binding.sale.visibility = View.VISIBLE

        when(ad.status){
            AVALIABLE -> {
                binding.point.setBackgroundResource(R.drawable.point_blue)
                binding.divider1.setBackgroundResource(android.R.color.darker_gray)
                binding.point2.setBackgroundResource(R.drawable.point_gray)
                binding.divider2.setBackgroundResource(android.R.color.darker_gray)
                binding.point3.setBackgroundResource(R.drawable.point_gray)
                binding.divider3.setBackgroundResource(android.R.color.darker_gray)
                binding.point4.setBackgroundResource(R.drawable.point_gray)
                binding.status.setText(AVALIABLE)
            }
            TALKING -> {
                binding.point.setBackgroundResource(R.drawable.point_green)
                binding.divider1.setBackgroundResource(android.R.color.holo_green_dark)
                binding.point2.setBackgroundResource(R.drawable.point_blue)
                binding.divider2.setBackgroundResource(android.R.color.darker_gray)
                binding.point3.setBackgroundResource(R.drawable.point_gray)
                binding.divider3.setBackgroundResource(android.R.color.darker_gray)
                binding.point4.setBackgroundResource(R.drawable.point_gray)
                binding.status.setText(TALKING)
            }
            TRANSPORT ->{
                binding.point.setBackgroundResource(R.drawable.point_green)
                binding.divider1.setBackgroundResource(android.R.color.holo_green_dark)
                binding.point2.setBackgroundResource(R.drawable.point_green)
                binding.divider2.setBackgroundResource(android.R.color.holo_green_dark)
                binding.point3.setBackgroundResource(R.drawable.point_blue)
                binding.divider3.setBackgroundResource(android.R.color.darker_gray)
                binding.point4.setBackgroundResource(R.drawable.point_gray)
                binding.status.setText(TRANSPORT)
            }
            CONCLUDED -> {
                binding.point.setBackgroundResource(R.drawable.point_green)
                binding.divider1.setBackgroundResource(android.R.color.holo_green_dark)
                binding.point2.setBackgroundResource(R.drawable.point_green)
                binding.divider2.setBackgroundResource(android.R.color.holo_green_dark)
                binding.point3.setBackgroundResource(R.drawable.point_green)
                binding.divider3.setBackgroundResource(android.R.color.holo_green_dark)
                binding.point4.setBackgroundResource(R.drawable.point_green)
                binding.status.setText(CONCLUDED)
            }
        }
        binding.backgroung.setBackgroundColor(
            when (ad?.type) {
                "Oferta" -> binding.backgroung.resources.getColor(R.color.green)
                else -> binding.backgroung.resources.getColor(R.color.blue)
            }
        )

    }

    private fun setUser(binding: AdDetailsBinding, user: User, img: RequestBuilder<Drawable>){
        img.into(binding!!.imgProfile)
        binding!!.imgProfile.visibility = View.VISIBLE
        binding.profileName.setText(user.name)
    }

    private fun feed(binding: AdDetailsBinding){
        binding!!.btRight.setImageResource(R.drawable.ic_mail)
        binding.btCenter.visibility = View.GONE
        binding.btLeft.setImageResource(R.drawable.ic_trash)
        binding.progress.visibility = View.GONE
    }

    private fun profile(binding: AdDetailsBinding){
        binding!!.btRight.setImageResource(R.drawable.ic_trash)
        binding.btCenter.visibility = View.GONE
        binding.btLeft.visibility = View.GONE
        binding.progress.visibility = View.GONE
        binding.toolbar.visibility = View.GONE
    }

    private fun done(binding: AdDetailsBinding, ad: Ad){

        when(ad.status){
            AVALIABLE -> {
                binding!!.btRight.setImageResource(R.drawable.ic_pencil)
                binding.btCenter.setImageResource(R.drawable.ic_rubbish)
                binding.btLeft.setImageResource(R.drawable.ic_next_disabled)
                binding.btLeft.isEnabled = false
            }
            TALKING -> {
                binding!!.btRight.setImageResource(R.drawable.ic_cancel)
                binding.btCenter.setImageResource(R.drawable.ic_mail)
                binding.btLeft.setImageResource(R.drawable.ic_next)
            }
            TRANSPORT ->{
                binding!!.btRight.setImageResource(R.drawable.ic_cancel)
                binding.btCenter.setImageResource(R.drawable.ic_mail)
                binding.btLeft.setImageResource(R.drawable.ic_tick)
            }
            CONCLUDED -> {
                binding!!.btRight.visibility = View.GONE
                binding.btCenter.visibility = View.GONE
                binding.btLeft.setImageResource(R.drawable.ic_refresh)
            }
        }

    }

    private fun answer(binding: AdDetailsBinding, ad: Ad){


        when(ad.status){
            CONCLUDED ->{
                binding!!.btRight.visibility = View.GONE
                binding.btCenter.visibility = View.GONE
                binding.btLeft.visibility = View.GONE

            }

            else->{
                binding!!.btRight.setImageResource(R.drawable.ic_cancel)
                binding.btCenter.visibility = View.GONE
                binding.btLeft.setImageResource(R.drawable.ic_mail)

            }
        }
    }

}


