package org.destinosustentavel.dsmovel.tools

import android.content.ContentValues
import android.content.Context
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import org.destinosustentavel.dsmovel.*
import org.destinosustentavel.dsmovel.model.*

class Repository {

    private var database: FirebaseFirestore
    private var auth: FirebaseAuth

    init {
        database = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
    }

    fun getAd(
        ad: Ad,
        _ads: MutableLiveData<Ad>,
        _user: MutableLiveData<User?>,
        swipe: SwipeRefreshLayout? = null
    ) {



        val query = FirebaseFirestore.getInstance().collection("ads")
            .document(ad.id!!)
        query.get()
            .addOnSuccessListener { result ->
                val ad = result.toObject(Ad::class.java)
                ad!!.id = result.id

                _ads.value = ad
                if(!ad.status.equals(AVALIABLE)){
                    ad.collect?.let {
                        FirebaseFirestore.getInstance().collection("user")
                            .document(it)
                            .get().addOnSuccessListener { doc ->
                                val user = doc.toObject(User::class.java)
                                user?.id = doc.id
                                _user.value = user
                                swipe?.isRefreshing = false
                            }
                    }
                }
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents.", exception)
                swipe?.isRefreshing = false
            }
    }

    fun getDone(
        _ads: MutableLiveData<ArrayList<Ad>>,
        _user: MutableLiveData<ArrayList<User?>>,
        _img: MutableLiveData<ArrayList<RequestBuilder<Drawable>?>>,
        context: Context,
        swipe: SwipeRefreshLayout? = null
    ) {



        val query = FirebaseFirestore.getInstance().collection("ads")
            .whereEqualTo("user_id", auth.uid)
            .orderBy("created_at", Query.Direction.DESCENDING)
            .orderBy("end", Query.Direction.DESCENDING)

        var adList = ArrayList<Ad>()
        var userList = ArrayList<User?>()
        var imgList = ArrayList<RequestBuilder<Drawable>?>()
        _user.value = userList
        _img.value = imgList
        _ads.value = adList
        query.get()
            .addOnSuccessListener { result ->
                for ((i, document) in result.withIndex()) {
                    val ad = document.toObject(Ad::class.java)
                    ad.id = document.id
                    if(
                        !ad.status!!.equals(CONCLUDED)
                            .or(ad.status!!.equals(DELETED))
                    ){

                        adList.add(ad)
                        userList.add(null)
                        imgList.add(null)
                        Log.d(ContentValues.TAG, "${document.id} => ${document.data}")
                    }
                }

                _user.value = userList
                _img.value = imgList
                _ads.value = adList

                for((i,ad) in adList.withIndex()){
                    if(!ad.status.equals(AVALIABLE)){
                        ad.collect?.let {
                            FirebaseFirestore.getInstance().collection("user")
                                .document(it)
                                .get().addOnSuccessListener { doc ->
                                    val user = doc.toObject(User::class.java)
                                    user?.id = doc.id
                                    userList.set(i,user)
                                    imgList.set(i, Glide
                                        .with(context)
                                        .load(user?.photo))

                                    _user.value = userList
                                    _img.value = imgList
                                    _ads.value = adList
                                    if (i == adList.size-1){
                                        Log.d(ContentValues.TAG, "Download finalizado ")
                                        swipe?.isRefreshing = false
                                    }
                                }
                        }
                    }
                }

                if(adList.size==0) {
                    swipe?.isRefreshing = false
                }
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents.", exception)

            }
    }

    fun getDoneConcluded(
        _ads: MutableLiveData<ArrayList<Ad>>,
        _user: MutableLiveData<ArrayList<User?>>,
        _img: MutableLiveData<ArrayList<RequestBuilder<Drawable>?>>,
        context: Context,
        swipe: SwipeRefreshLayout? = null
    ) {



        val query = FirebaseFirestore.getInstance().collection("ads")
            .whereEqualTo("user_id", auth.uid.toString())
            .whereEqualTo("status", CONCLUDED)
            .orderBy("created_at", Query.Direction.DESCENDING)
            .orderBy("end", Query.Direction.DESCENDING)

        var adList = ArrayList<Ad>()
        var userList = ArrayList<User?>()
        var imgList = ArrayList<RequestBuilder<Drawable>?>()
        _user.value = userList
        _img.value = imgList
        _ads.value = adList
        query.get()
            .addOnSuccessListener { result ->
                for ((i, document) in result.withIndex()) {
                    val ad = document.toObject(Ad::class.java)
                    ad.id = document.id

                    adList.add(ad)
                    userList.add(null)
                    imgList.add(null)
                    Log.d(ContentValues.TAG, "${document.id} => ${document.data}")

                }

                _user.value = userList
                _img.value = imgList
                _ads.value = adList

                for((i,ad) in adList.withIndex()){
                    if(!ad.status.equals(AVALIABLE)){
                        ad.collect?.let {
                            FirebaseFirestore.getInstance().collection("user")
                                .document(it)
                                .get().addOnSuccessListener { doc ->
                                    val user = doc.toObject(User::class.java)
                                    user?.id = doc.id
                                    userList.set(i,user)
                                    imgList.set(i, Glide
                                        .with(context)
                                        .load(user?.photo))

                                    _user.value = userList
                                    _img.value = imgList
                                    _ads.value = adList
                                    if (i == adList.size-1){
                                        Log.d(ContentValues.TAG, "Download finalizado ")
                                        swipe?.isRefreshing = false
                                    }
                                }
                        }
                    }
                }

                if(adList.size==0) {
                    swipe?.isRefreshing = false
                }
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents.", exception)

            }
    }

    fun getAnswer(
        _ads: MutableLiveData<ArrayList<Ad>>,
        _user: MutableLiveData<ArrayList<User?>>,
        _img: MutableLiveData<ArrayList<RequestBuilder<Drawable>?>>,
        context: Context,
        swipe: SwipeRefreshLayout? = null
    ) {



        val query = FirebaseFirestore.getInstance().collection("ads")
            .whereEqualTo("collect", auth.uid.toString())
            .orderBy("created_at", Query.Direction.DESCENDING)
            .orderBy("end", Query.Direction.DESCENDING)
        var adList = ArrayList<Ad>()
        var userList = ArrayList<User?>()
        var imgList = ArrayList<RequestBuilder<Drawable>?>()
        query.get()
            .addOnSuccessListener { result ->
                for ((i, document) in result.withIndex()) {
                    var ad: Ad = Ad()
                    ad = document.toObject(Ad::class.java)
                    if(!ad.status.equals(CONCLUDED).or(ad.status.equals(DELETED))){
                        ad.id = document.id
                        adList.add(ad)
                        userList.add(null)
                        imgList.add(null)

                    }
                }
                _user.value = userList
                _img.value = imgList
                _ads.value = adList
                for((i,ad) in adList.withIndex()){
                    FirebaseFirestore.getInstance().collection("user")
                        .document(ad?.user_id.toString())
                        .get().addOnSuccessListener { doc ->
                            val user = doc.toObject(User::class.java)
                            user?.id = doc.id
                            userList.set(i,user)
                            imgList.set(i, Glide
                                .with(context)
                                .load(user?.photo))

                            _user.value = userList
                            _img.value = imgList
                            _ads.value = adList
                            Log.d(ContentValues.TAG, "${doc.id} => ${doc.data}")
                            if (i == adList.size-1){
                                Log.d(ContentValues.TAG, "Download finalizado ")
                                swipe?.isRefreshing = false
                            }
                        }
                }

                if(adList.size==0) {
                    swipe?.isRefreshing = false
                }
            }
            .addOnFailureListener { exception ->
                swipe?.isRefreshing = false
                Log.w(ContentValues.TAG, "Error getting documents.", exception)

            }

    }

    fun getAnswerConcluded(
        _ads: MutableLiveData<ArrayList<Ad>>,
        _user: MutableLiveData<ArrayList<User?>>,
        _img: MutableLiveData<ArrayList<RequestBuilder<Drawable>?>>,
        context: Context,
        swipe: SwipeRefreshLayout? = null
    ) {



        val query = FirebaseFirestore.getInstance().collection("ads")
            .whereEqualTo("collect", auth.uid.toString().toString())
            .whereEqualTo("status", CONCLUDED)
            .orderBy("created_at", Query.Direction.DESCENDING)
            .orderBy("end", Query.Direction.DESCENDING)
        var adList = ArrayList<Ad>()
        var userList = ArrayList<User?>()
        var imgList = ArrayList<RequestBuilder<Drawable>?>()
        query.get()
            .addOnSuccessListener { result ->
                for ((i, document) in result.withIndex()) {
                    var ad: Ad = Ad()
                    ad = document.toObject(Ad::class.java)
                    ad.id = document.id
                    adList.add(ad)
                    userList.add(null)
                    imgList.add(null)
                }

                _user.value = userList
                _img.value = imgList
                _ads.value = adList
                for((i,ad) in adList.withIndex()){
                    FirebaseFirestore.getInstance().collection("user")
                        .document(ad?.user_id.toString())
                        .get().addOnSuccessListener { doc ->
                            val user = doc.toObject(User::class.java)
                            user?.id = doc.id
                            userList.set(i,user)
                            imgList.set(i, Glide
                                .with(context)
                                .load(user?.photo))

                            _user.value = userList
                            _img.value = imgList
                            _ads.value = adList
                            Log.d(ContentValues.TAG, "${doc.id} => ${doc.data}")
                            if (i == adList.size-1){
                                Log.d(ContentValues.TAG, "Download finalizado ")
                                swipe?.isRefreshing = false
                            }
                        }
                }

                if(adList.size==0) {
                    swipe?.isRefreshing = false
                }
            }
            .addOnFailureListener { exception ->
                swipe?.isRefreshing = false
                Log.w(ContentValues.TAG, "Error getting documents.", exception)

            }

    }

    fun getPublicAds(
        _ads: MutableLiveData<ArrayList<Ad>>,
        _user: MutableLiveData<ArrayList<User?>>,
        _img: MutableLiveData<ArrayList<RequestBuilder<Drawable>?>>,
        context: Context,
        swipe: SwipeRefreshLayout? = null
    ) {
        val query = FirebaseFirestore.getInstance().collection("ads")
            .orderBy("created_at", Query.Direction.DESCENDING)
            .whereEqualTo("status", AVALIABLE)
            .limit(500)
        var adList = ArrayList<Ad>()
        var userList = ArrayList<User?>()
        var imgList = ArrayList<RequestBuilder<Drawable>?>()
        query.get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    var ad = document.toObject(Ad::class.java)
                    if(!ad.user_id!!.equals(
                            auth.uid)){
                        ad.id = document.id
                        adList.add(ad)
                        userList.add(null)
                        imgList.add(null)

                    }
                }


                _user.value = userList
                _img.value = imgList
                _ads.value = adList
                for((i,ad) in adList.withIndex()){
                    FirebaseFirestore.getInstance().collection("user")
                        .document(ad?.user_id.toString())
                        .get().addOnSuccessListener { doc ->
                            val user = doc.toObject(User::class.java)
                            user?.id = doc.id
                            userList.set(i,user)
                            imgList.set(i, Glide
                                .with(context)
                                .load(user?.photo))

                            _user.value = userList
                            _img.value = imgList
                            _ads.value = adList
                            Log.d(ContentValues.TAG, "${doc.id} => ${doc.data}")
                            if (i == adList.size-1){
                                Log.d(ContentValues.TAG, "Download finalizado ")
                                swipe?.isRefreshing = false
                            }
                        }
                }

                if(adList.size==0) {
                    swipe?.isRefreshing = false
                }
            }
            .addOnFailureListener { exception ->
                swipe?.isRefreshing = false
                Log.w(ContentValues.TAG, "Error getting documents.", exception)

            }

    }


    fun getAdProfile(
        _ads: MutableLiveData<ArrayList<Ad>>,
        email: String,
        _img: MutableLiveData<RequestBuilder<Drawable>>,
        swipe: SwipeRefreshLayout? = null
    ) {

        var list = ArrayList<Ad>()

        val query = FirebaseFirestore.getInstance().collection("ads")
            .whereEqualTo("user_id", email)
            .whereEqualTo("status", AVALIABLE)
            .orderBy("created_at", Query.Direction.DESCENDING)
            .orderBy("end", Query.Direction.DESCENDING)

        query.get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    var ad: Ad = Ad()
                    ad = document.toObject(Ad::class.java)
                    ad.id = document.id
                    list.add(ad)
                    Log.d(ContentValues.TAG, "${document.id} => ${document.data}")

                }
                swipe?.isRefreshing = false
                _ads.value = list
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents.", exception)

            }
    }



    fun processing(getAd: Ad, action: String, _status: MutableLiveData<Int>? = null) {

        var database = FirebaseFirestore.getInstance()

        database
            .collection("ads")
            .document(getAd?.id!!)
            .get()
            .addOnSuccessListener {

                var status = it.getString("status")


                when(action){
                    ABORT -> {
                        database
                            .collection("ads")
                            .document(getAd?.id!!)
                            .update(hashMapOf<String,String>("collect" to "",
                                "status" to AVALIABLE
                            ) as Map<String, Any>)
                            .addOnSuccessListener {
                                addNotification(
                                    auth.uid.toString()!!,
                                    getAd.user_id!!,
                                    ABORT,
                                    getAd.id!!,
                                    _status
                                )
                            }
                    }
                    AVALIABLE -> {
                        database
                            .collection("ads")
                            .document(getAd?.id!!)
                            .update(hashMapOf<String,String>("collect" to "",
                                "status" to AVALIABLE
                            ) as Map<String, Any>)
                            .addOnSuccessListener {
                                addNotification(
                                    auth.uid.toString()!!,
                                    getAd.collect!!,
                                    ABORT,
                                    getAd.id!!,
                                    _status
                                )
                            }
                    }
                    TALKING -> {
                        if(status== AVALIABLE){
                            database
                                .collection("ads")
                                .document(getAd?.id!!)
                                .update(hashMapOf<String,String>("collect" to auth.uid.toString()!!,
                                    "status" to TALKING
                                ) as Map<String, Any>)
                                .addOnSuccessListener {
                                    addNotification(
                                        auth.uid.toString()!!,
                                        getAd.user_id!!,
                                        TALKING,
                                        getAd.id!!,
                                        _status
                                    )
                                }
                        } else if(auth.uid.toString()==status){
                            _status?.value = 1
                        }
                        else {
                            _status?.value = 2
                        }
                    }
                    TRANSPORT -> {
                        database
                            .collection("ads")
                            .document(getAd?.id!!)
                            .update("status", TRANSPORT)
                            .addOnSuccessListener {
                                addNotification(
                                    auth.uid.toString()!!,
                                    getAd.collect!!,
                                    TRANSPORT,
                                    getAd.id!!,
                                    _status
                                )
                            }
                    }

                    CONCLUDED -> {
                        database
                            .collection("ads")
                            .document(getAd?.id!!)
                            .update("status", CONCLUDED)
                            .addOnSuccessListener {
                                addNotification(
                                    auth.uid.toString()!!,
                                    getAd.collect!!,
                                    CONCLUDED,
                                    getAd.id!!,
                                    _status
                                )
                            }
                    }

                }

            }
    }

    fun addNotification(
        fromUser: String,
        toUser: String,
        action: String,
        adId: String,
        _status: MutableLiveData<Int>?
    ){
        database
            .collection("user")
            .document(toUser)
            .collection("notifications")
            .add(
                Notification()
                    .apply {
                        from = fromUser
                        this.action = action
                        this.adId = adId
                        created = Timestamp.now().toDate()
                    })
            .addOnSuccessListener {
                _status?.value = 0

            }
    }

    fun getNotification(id: String,
                        notification: MutableLiveData<ArrayList<Notification>>,
                        ads: MutableLiveData<ArrayList<Ad>>,
                        user: MutableLiveData<ArrayList<User>>
    ){

        var noti = ArrayList<Notification>()
        var adsList = ArrayList<Ad>()
        var users = ArrayList<User>()
        database.collection("user")
            .document(id)
            .collection("notifications")
            .orderBy("created", Query.Direction.ASCENDING)
            .limit(50)
            .addSnapshotListener{ querySnapshot, firebaseFirestoreException ->
                var docs = querySnapshot?.documentChanges
                docs?.run{
                    for( (i, doc) in withIndex()){
                        if(doc.type==DocumentChange.Type.ADDED){
                            var notificationAux = doc.document.toObject(Notification::class.java)
                            noti.add(0,notificationAux)
                            adsList.add(Ad())
                            users.add(User())

                        }
                    }
                    notification.value = noti
                    ads.value = adsList
                    user.value = users
                    for((i, notificationAux) in noti.withIndex()){

                        database.collection("user")
                            .document(notificationAux.from!!)
                            .get()
                            .addOnSuccessListener {
                                val userAux = it.toObject(User::class.java)
                                userAux?.id = it.id
                                users.set(i,userAux!!)
                                user.value = users
                                database.collection("ads")
                                    .document(notificationAux.adId!!)
                                    .get()
                                    .addOnSuccessListener {adDoc ->
                                        adDoc.toObject(Ad::class.java)?.let { ad ->
                                            adsList.set(i, ad)
                                            ads.value = adsList
                                        }
                                    }
                            }
                    }
                }
            }
    }


    fun listenerMessageConversation(
        toId: String,
        _messagen: MutableLiveData<Message>
    ) {
        FirebaseFirestore.getInstance()
            .collection("user")
            .document(FirebaseAuth.getInstance().uid.toString())
            .collection(toId)
            .orderBy("timestamp", Query.Direction.ASCENDING)
            .limit(1000)
            .addSnapshotListener { querySnapshot, firebaseFirestoreException ->
                var listMsn = ArrayList<Message>()
                var docs = querySnapshot?.documentChanges
                if (docs != null) {
                    for(doc in docs){
                        val messagen = doc.document.toObject(Message::class.java)
                        listMsn.add(0, messagen)
                        _messagen.value = messagen

                    }
                }
            }
    }

    fun listenerLastMessagen(_messages: MutableLiveData<ArrayList<Message>>,
                             _user: MutableLiveData<ArrayList<User>>) {
        FirebaseFirestore.getInstance()
            .collection("user")
            .document(FirebaseAuth.getInstance().uid.toString())
            .collection("last-message")
            .orderBy("timestamp", Query.Direction.ASCENDING)
            .addSnapshotListener { querySnapshot, ex ->
                var listMsn: ArrayList<Message> = ArrayList()
                _messages.value?.let { listMsn = it }
                var listUser: ArrayList<User> = ArrayList()
                _user.value?.let { listUser = it }
                var docs = querySnapshot?.documentChanges
                if (docs != null) {
                    for (doc in docs) {
                        if(doc.type==DocumentChange.Type.ADDED){
                            val messagen = doc.document.toObject(Message::class.java)
                            database.collection("user")
                                .document(doc.document.id!!)
                                .get()
                                .addOnSuccessListener {
                                    listMsn.add(0, messagen)
                                    val userAux = it.toObject(User::class.java)
                                    userAux?.id = it.id
                                    listUser.add(0, userAux!!)
                                    _user.value = listUser
                                    _messages.value = listMsn
                                }
                        } else if(doc.type==DocumentChange.Type.MODIFIED){

                            val messagen = doc.document.toObject(Message::class.java)

                            for((i, user) in _user.value!!.withIndex()){
                                if((user.id==doc.document.id).and(i!=0)){
                                    listUser.removeAt(i)
                                    listMsn.removeAt(i)
                                    listUser.add(0, user)
                                    listMsn.add(0, messagen)
                                }
                            }
                            _user.value = listUser
                            _messages.value = listMsn
                        }
                    }
                }
            }
    }

    fun send(message: Message) {

        database
            .collection("user")
            .document(message.fromId!!)
            .collection(message.toId!!)
            .add(message)
            .addOnSuccessListener {
                database
                    .collection("user")
                    .document(message.fromId!!)
                    .collection("last-message")
                    .document(message.toId!!)
                    .set(message)
            }

        database
            .collection("user")
            .document(message.toId!!)
            .collection(message.fromId!!)
            .add(message)
            .addOnSuccessListener {

                database
                    .collection("user")
                    .document(message.toId!!)
                    .collection("last-message")
                    .document(message.fromId!!)
                    .set(message)
            }
    }

}