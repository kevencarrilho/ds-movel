package org.destinosustentavel.dsmovel.tools

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import org.destinosustentavel.dsmovel.model.Ad
import org.destinosustentavel.dsmovel.model.User

class LocationListener(private val context: Context, private var user: User? = null, private var ad: Ad? = null): LocationListener {

    private var windows: AlertDialog

    init {
        windows = AlertDialog.Builder(context).setTitle("Carregamento de endereço")
            .setMessage("Peço que tenha um pouco de calma, estamos usando o GPS para" +
                    " encontrar o seu endereço, logo aparecerá" +
                    " uma janela com o endereço encontrado!")
            .setPositiveButton("Ok", null).create()
        windows.show()
    }

    override fun onLocationChanged(location: Location?) {

        windows.dismiss()
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        if (isConnected) {
            location?.let{ p0->
                var address = getAddress(p0)
                user?.let {
                    it.latitude = p0.latitude
                    it.longitude =  p0.longitude
                }
                ad?.let {
                    it.latitude = p0.latitude
                    it.longitude =  p0.longitude
                }
                windows = AlertDialog.Builder(context).setTitle("Seu endereço")
                    .setMessage("O endereço que será usado para essa operação é " +
                            address?.getAddressLine(0) +
                            ".")
                    .setPositiveButton("Ok",null)
                    .create()

                windows.show()

            }
        } else{
            windows = AlertDialog.Builder(context).setTitle("Sem Internet")
                .setMessage("Para atualizar o endereço é preciso ter conexão com a internet!")
                .create()
            windows.show()
        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onProviderEnabled(provider: String?) {

    }

    override fun onProviderDisabled(provider: String?) {

    }
    private fun getAddress(location: Location): Address?{
        val geo = Geocoder(context)
        val addresses = geo.getFromLocation(location.latitude,location.longitude,1)
        if (addresses.size>0){
            return  addresses.get(0)
        } else return  null
    }
}