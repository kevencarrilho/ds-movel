package org.destinosustentavel.dsmovel.tools

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.details_ad.view.*
import org.destinosustentavel.dsmovel.*
import org.destinosustentavel.dsmovel.databinding.NotificationBinding
import org.destinosustentavel.dsmovel.model.Ad
import org.destinosustentavel.dsmovel.model.Notification
import org.destinosustentavel.dsmovel.model.User
import java.text.SimpleDateFormat

class AdapterNotifications(
    var ads: ArrayList<Ad>,
    var notifications: ArrayList<Notification>,
    var users: ArrayList<User>
) : RecyclerView.Adapter<AdapterNotifications.ViewHolder>() {


    var toGo: ((Ad, User) -> Unit)? = null

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val b: NotificationBinding?
        init {
            b = DataBindingUtil.bind(itemView)
            b?.layoutNotify?.setOnClickListener{
                toGo?.invoke(ads.get(adapterPosition), users.get(adapterPosition))
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_notify, parent, false))

    }

    override fun getItemCount(): Int {
        return ads.count()
    }

    override fun onBindViewHolder(h: ViewHolder, position: Int) {
        ads.get(position)?.let{ad ->
            users.get(position)?.let{user ->
                notifications.get(position)?.let { notification ->
                    h.b!!.imageAd.setImageResource(
                        when (ad.material_type) {
                            "Plastico" -> R.drawable.ic_plastic_recycler
                            "Metal" -> R.drawable.ic_metal_recycler
                            "Vidro" -> R.drawable.ic_glass_recycler
                            "Papel" -> R.drawable.ic_paper_recycler
                            "Eletrônico" -> R.drawable.ic_ewaste_recycler
                            else -> R.drawable.ic_organic_recycler
                        }
                    )
                    h.b.date.setText(SimpleDateFormat("dd/MM/yyyy")
                        .format(notification.created).toString())
                    Glide.with(h.b!!.imageUser)
                        .load(user.photo)
                        .into(h.b!!.imageUser)
                    when(notification.action){
                        ABORT -> {
                            h.b.title.setText("Usuário desistiu")
                            h.b.message.setText(user.name + " desistiu de atender o anúncio " + ad.title)
                        }
                        TALKING -> {
                            h.b.title.setText("Anúncio atendido")
                            h.b.message.setText(user.name + " atendeu o anúncio " + ad.title)
                        }
                        TRANSPORT ->  {
                            h.b.title.setText("Disponível para transporte")
                            h.b.message.setText(user.name + " está esperando o tranporte do material no anúncio " + ad.title)
                        }
                        CONCLUDED -> {
                            h.b.title.setText("Anúncio concluído")
                            h.b.message.setText(user.name + " concluiu o anúncio " + ad.title)
                        }
                    }

                }
            }
        }
    }


}