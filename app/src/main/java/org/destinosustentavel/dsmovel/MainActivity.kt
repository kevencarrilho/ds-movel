package org.destinosustentavel.dsmovel

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.iid.FirebaseInstanceId
import org.destinosustentavel.dsmovel.model.User
import org.destinosustentavel.dsmovel.vm.*


class MainActivity : AppCompatActivity() {

    private lateinit var ads: FeedViewModel
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val user: UserViewModel by lazy {
        ViewModelProvider(this).get(UserViewModel::class.java)
    }


    // -------------------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findNavController(R.id.nav_host_fragment)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        this.supportActionBar
        user.set(intent.getParcelableExtra("user"))
        ads = ViewModelProvider(this).get(FeedViewModel::class.java)
        ViewModelProvider(this).get(LastMessageViewModel::class.java)
        ViewModelProvider(this).get(DoneViewModel::class.java)
        ViewModelProvider(this).get(DoneConcludedViewModel::class.java)
        ViewModelProvider(this).get(AnswerViewModel::class.java)
        ViewModelProvider(this).get(AnswerConcludedViewModel::class.java)
        val factory = NotificationsViewModel.Factory(application,FirebaseAuth.getInstance().uid!!)
        ViewModelProvider(this,factory).get(NotificationsViewModel::class.java)
        ViewModelProvider(this).get(PlaceViewModel::class.java)
        ViewModelProvider(this).get(SettingViewModel::class.java)

        setConfirm()

        updateToken()

    }

    private fun updateToken() {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {

            val newToken = it.token
            FirebaseFirestore.getInstance()
                .collection("user").document(FirebaseAuth.getInstance().uid!!)
                .get().addOnSuccessListener {
                    val user = it.toObject(User::class.java)
                    var new = true
                    for(tok in user?.token!!){
                        if(tok.equals(newToken)) new = false
                    }

                    if(new){
                        user.token.add(newToken)
                        FirebaseFirestore.getInstance().collection("user")
                            .document(FirebaseAuth.getInstance().uid!!).set(user)
                            .addOnSuccessListener {
                                Log.e("ERROR UPDATE TOKEN", "UPDATE FEITO COM SUCESSO")
                            }.addOnFailureListener {
                                Log.e("ERROR UPDATE TOKEN", it.message)
                            }
                    }
                }

        }
    }

    private fun setConfirm() {
        ads.status.observe(this, Observer {
            when(it){
                0 -> {
                    AlertDialog.Builder(this)
                        .setTitle("Feito!")
                        .setMessage("O responsável pelo anúncio foi notificado, aguarde o contato.")
                        .setPositiveButton("Ok", null)
                        .create().show()
                    ads.resetStatus()
                }
                1 -> {
                    AlertDialog.Builder(this)
                        .setTitle("Ops!")
                        .setMessage("Você já atendeu esse chamado, aguarde o anunciante entrar em contato.")
                        .setPositiveButton("Ok", null)
                        .create().show()
                    ads.resetStatus()
                }
                2 -> {
                    AlertDialog.Builder(this)
                        .setTitle("Desculpa, mas...")
                        .setMessage("Esse anúncio já foi atendido por outro usuário.")
                        .setPositiveButton("Ok", null)
                        .create().show()
                    ads.resetStatus()
                }
            }
        })
    }

//    override fun onResume() {
//        super.onResume()
//        online(true)
//    }
//
//    override fun onPause() {
//        super.onPause()
//        online(false)
//    }
//
//
//    private fun online(status: Boolean){
//        FirebaseFirestore.getInstance().collection("user")
//            .document(currentUser?.email!!)
//            .update("online", status)
//    }
}


