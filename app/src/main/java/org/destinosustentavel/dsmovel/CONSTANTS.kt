package org.destinosustentavel.dsmovel

const val TYPE = "TYPE"
const val NEW_CREATE_USER = "new"
const val ARG_CREATE_AD = "ad"
const val DONE = "Realizando"
const val ANSWER = "Atendendo"
const val ORGANIZATION = "Organização"
const val USER = "user"
const val AVALIABLE = "Disponível"
const val TALKING = "Conversando"
const val TRANSPORT = "Transportando"
const val CONCLUDED = "Concluído"
const val DELETED = "Deletado"
const val ABORT = "Desistir"
const val RENEW = "RENEW"
const val AD_FLAG = "AD_FLAG"
const val AD_FEED = 0
const val AD_DONE = 1
const val AD_ANSWER = 2
const val AD_PROFILE = 3