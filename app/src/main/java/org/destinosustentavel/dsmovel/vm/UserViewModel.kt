package org.destinosustentavel.dsmovel.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import org.destinosustentavel.dsmovel.model.User

class UserViewModel : ViewModel(), ViewModelProvider.Factory {
    // TODO: Implement the ViewModel
    private var database: FirebaseFirestore
    private var auth: FirebaseAuth
    init {
        database = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
    }

    fun set(user: User){
        _user.value = user
    }

    fun save(user: User){
        database.collection("user")
            .document(auth.uid.toString())
            .set(user)
    }
    fun update(){
        database.collection("user")
            .document(auth.uid.toString())
            .set(user.value!!)
    }
    fun get(email: String){
        database.collection("user")
            .document(email)
            .get().addOnSuccessListener {  }
    }

    private val _user = MutableLiveData<User>().apply {
    }
    val user: LiveData<User> = _user

    fun setList(users: MutableLiveData<User>){
        _selectUser = users
    }
    private var _selectUser = MutableLiveData<User>().apply {
    }
    val listUser: LiveData<User> = _selectUser
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
       return modelClass.newInstance()
    }

}
