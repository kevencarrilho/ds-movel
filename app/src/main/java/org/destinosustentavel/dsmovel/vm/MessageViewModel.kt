package org.destinosustentavel.dsmovel.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.destinosustentavel.dsmovel.model.Message
import org.destinosustentavel.dsmovel.tools.Repository
import javax.inject.Inject

class MessageViewModel(private val toId :String) : ViewModel(){

    private val repo: Repository

    init {
        repo = Repository()
    }
    private var _messagen = MutableLiveData<Message>().apply {
        repo.listenerMessageConversation(toId, this)
    }
    var messagen: LiveData<Message> = _messagen
    private var _messages = MutableLiveData<ArrayList<Message>>().apply {
    }
    var messages: LiveData<ArrayList<Message>> = _messages

    fun send(message: Message) {
        repo.send(message)
    }

    fun addMessagem() {
        _messages.value?.add(_messagen.value!!)
    }


    class Factory @Inject constructor(private val toId :String) :
        ViewModelProvider.Factory {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return if (modelClass.isAssignableFrom(MessageViewModel::class.java!!)) {
                MessageViewModel(toId) as T
            } else {
                throw IllegalArgumentException("ViewModel Not Found")
            }
        }

    }
}
