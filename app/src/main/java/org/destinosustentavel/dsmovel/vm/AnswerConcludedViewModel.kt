package org.destinosustentavel.dsmovel.vm

import android.app.Application
import android.graphics.drawable.Drawable
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.RequestBuilder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import org.destinosustentavel.dsmovel.ABORT
import org.destinosustentavel.dsmovel.AVALIABLE
import org.destinosustentavel.dsmovel.model.Ad
import org.destinosustentavel.dsmovel.model.User
import org.destinosustentavel.dsmovel.tools.Repository
import kotlin.collections.ArrayList


class AnswerConcludedViewModel(application: Application) : AndroidViewModel(application) {


    private var database: FirebaseFirestore
    private var auth: FirebaseAuth
    private var currentUser: FirebaseUser?
    private val repo: Repository


    init {
        database = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
        currentUser = auth.currentUser
        repo = Repository()
    }
    private val _user = MutableLiveData<ArrayList<User?>>()
    private val _img = MutableLiveData<ArrayList<RequestBuilder<Drawable>?>>()
    private val _ads = MutableLiveData<ArrayList<Ad>>().apply {
        repo.getAnswerConcluded(this, _user, _img, getApplication())
    }

    val user: LiveData<ArrayList<User?>> = _user
    val img: LiveData<ArrayList<RequestBuilder<Drawable>?>> = _img
    var ads: LiveData<ArrayList<Ad>> = _ads

    fun processing(position: Int){
        repo.processing(_ads.value?.get(position)!!, ABORT)
        _ads.value?.removeAt(position)
        _ads.value = _ads.value
    }

    fun update(swipe: SwipeRefreshLayout) {
        repo.getAnswerConcluded(_ads, _user, _img, getApplication(), swipe)
    }

}