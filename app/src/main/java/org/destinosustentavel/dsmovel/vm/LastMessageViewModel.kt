package org.destinosustentavel.dsmovel.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.destinosustentavel.dsmovel.model.*
import org.destinosustentavel.dsmovel.tools.Repository

class LastMessageViewModel : ViewModel(), ViewModelProvider.Factory {
    private var repo: Repository
    init {
        repo = Repository()
    }

    private val _user = MutableLiveData<ArrayList<User>>() .apply {
        value = ArrayList()
    }
    val user: LiveData<ArrayList<User>> = _user

    private val _messagens = MutableLiveData<ArrayList<Message>>().apply {

        repo.listenerLastMessagen(this,_user)
    }
    var messagens: LiveData<ArrayList<Message>> = _messagens


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return  modelClass.newInstance()
    }

}
