package org.destinosustentavel.dsmovel.vm

import android.app.Application
import android.graphics.drawable.Drawable
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.RequestBuilder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import org.destinosustentavel.dsmovel.TALKING
import org.destinosustentavel.dsmovel.model.Ad
import org.destinosustentavel.dsmovel.model.User
import org.destinosustentavel.dsmovel.tools.Repository

class FeedViewModel (application: Application) : AndroidViewModel(application) {

    private var database: FirebaseFirestore
    private var auth: FirebaseAuth
    private val repo: Repository

    init {
        database = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
        repo = Repository()
    }
    private val _user = MutableLiveData<ArrayList<User?>>().apply {
        value = ArrayList()
    }
    private val _img = MutableLiveData<ArrayList<RequestBuilder<Drawable>?>>().apply {
        value = ArrayList()
    }
    private val _ads = MutableLiveData<ArrayList<Ad>>().apply {
        repo.getPublicAds(this, _user, _img, getApplication())
    }

    val user: LiveData<ArrayList<User?>> = _user
    val img: LiveData<ArrayList<RequestBuilder<Drawable>?>> = _img
    var ads: LiveData<ArrayList<Ad>> = _ads

    private val _status = MutableLiveData<Int>().apply {
        value = -1
    }

    fun resetStatus(){
        _status.value = -1
    }

    var status: LiveData<Int> = _status

    fun toTalk(getAd: Ad) {
        repo.processing(getAd, TALKING, _status)
    }

    fun uptadeAds(swipe: SwipeRefreshLayout? = null){
        repo.getPublicAds(_ads, _user, _img, getApplication(), swipe)
    }
}