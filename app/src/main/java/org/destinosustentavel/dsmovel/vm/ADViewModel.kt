package org.destinosustentavel.dsmovel.vm

import android.app.Application
import android.content.ContentValues.TAG
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.lifecycle.*
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import org.destinosustentavel.dsmovel.ABORT
import org.destinosustentavel.dsmovel.AVALIABLE
import org.destinosustentavel.dsmovel.DELETED
import org.destinosustentavel.dsmovel.TALKING
import org.destinosustentavel.dsmovel.model.Ad
import org.destinosustentavel.dsmovel.model.User
import org.destinosustentavel.dsmovel.tools.Repository
import javax.inject.Inject
import kotlin.collections.ArrayList


class AdViewModel(
    application: Application,
    private val ad: Ad,
    private val userIt: User?
) : AndroidViewModel(application) {


    private var database: FirebaseFirestore
    private var auth: FirebaseAuth
    private var currentUser: FirebaseUser?
    private val repo: Repository


    init {
        database = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
        currentUser = auth.currentUser
        repo = Repository()
    }
    private val _user = MutableLiveData<User?>().apply {
        value =  userIt
    }
    private val _ads = MutableLiveData<Ad>().apply {
        value =  ad
    }

    val user: LiveData<User?> = _user
    var ads: LiveData<Ad> = _ads





    fun processing(status: String){
        repo.processing(ad,status)
        ad.status = status
        if(status== ABORT){
            ad.status = AVALIABLE
            _user.value = null
            database.collection("ads").document(ad.id!!)
                .update("collect", null)
        }
    }

    fun delete(){
        database.collection("ads").document(ad.id!!)
            .update("status", DELETED)
    }


    fun editDone(ad: Ad){
        database.collection("ads").document(ad.id!!)
            .set(ad)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference}")
            }
            .addOnFailureListener { e ->

                Log.w(TAG, "Error adding document", e)
            }

    }

    fun updateDone(swipe: SwipeRefreshLayout){
        repo.getAd(ad, _ads, _user, swipe)
    }

    private val _status = MutableLiveData<Int>().apply {
        value = -1
    }

    var status: LiveData<Int> = _status

    fun toTalk(getAd: Ad) {
        repo.processing(getAd, TALKING, _status)
    }

    fun resetStatus(){
        _status.value = -1
    }
    class Factory @Inject constructor(private var app: Application,
                                      private val ad: Ad,
                                      private val userIt: User) :
        ViewModelProvider.Factory {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return if (modelClass.isAssignableFrom(AdViewModel::class.java!!)) {
                AdViewModel(application = app, ad = ad, userIt = userIt) as T
            } else {
                throw IllegalArgumentException("ViewModel Not Found")
            }
        }

    }

}