package org.destinosustentavel.dsmovel.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore

class SettingViewModel : ViewModel() {

    private var database: FirebaseFirestore
    private var auth: FirebaseAuth
    private val countDemand = MutableLiveData<Int>()
    private val countOffer = MutableLiveData<Int>()

    init {
        database = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
        updateCount()
    }


    fun updateCount(swipeRefreshLayout: SwipeRefreshLayout? = null){

        database.collection("ads")
            .whereEqualTo("user_id", auth.uid)
            .whereEqualTo("type", "Oferta")
            .get()
            .addOnSuccessListener {
                countOffer.value = it.size()
                database.collection("ads")
                    .whereEqualTo("user_id", auth.uid)
                    .whereEqualTo("type", "Pedido")
                    .get()
                    .addOnSuccessListener {
                        countDemand.value = it.size()
                        if (swipeRefreshLayout != null) {
                            swipeRefreshLayout.isRefreshing = false
                        }
                    }
            }
    }



    val offer: LiveData<Int> = countOffer

    val demand: LiveData<Int> = countDemand

}