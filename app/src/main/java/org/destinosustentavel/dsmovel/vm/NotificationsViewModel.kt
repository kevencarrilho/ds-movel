package org.destinosustentavel.dsmovel.vm

import android.app.Application
import androidx.lifecycle.*
import org.destinosustentavel.dsmovel.model.Ad
import org.destinosustentavel.dsmovel.model.Notification
import org.destinosustentavel.dsmovel.model.User
import org.destinosustentavel.dsmovel.tools.Repository
import javax.inject.Inject

class NotificationsViewModel(application: Application,
                             var id:  String): AndroidViewModel(application) {

    private val repo: Repository

    init {
        repo = Repository()
    }

    private val _notification = MutableLiveData<ArrayList<Notification>>()
    private val _user = MutableLiveData<ArrayList<User>>()
    private val _ads = MutableLiveData<ArrayList<Ad>>().apply {
        repo.getNotification(id,notification = _notification, ads = this, user = _user)
    }

    val notification: LiveData<ArrayList<Notification>> = _notification
    val ads: LiveData<ArrayList<Ad>> = _ads
    val user: LiveData<ArrayList<User>> = _user




    class Factory @Inject constructor(private var app: Application, private var id: String) :
        ViewModelProvider.Factory {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return if (modelClass.isAssignableFrom(NotificationsViewModel::class.java!!)) {
                NotificationsViewModel(application = app,id = this.id) as T
            } else {
                throw IllegalArgumentException("ViewModel Not Found")
            }
        }

    }
}