package org.destinosustentavel.dsmovel.vm

import android.app.Application
import android.graphics.drawable.Drawable
import androidx.lifecycle.*
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.RequestBuilder
import com.google.firebase.firestore.FirebaseFirestore
import org.destinosustentavel.dsmovel.TALKING
import org.destinosustentavel.dsmovel.model.Ad
import org.destinosustentavel.dsmovel.model.User
import org.destinosustentavel.dsmovel.tools.Repository
import javax.inject.Inject

class ProfileViewModel(application: Application,
                       var uid:  String): AndroidViewModel(application) {


    private val repo: Repository
    private var database: FirebaseFirestore
    private val countDemand = MutableLiveData<Int>()
    private val countOffer = MutableLiveData<Int>()

    init {
        database = FirebaseFirestore.getInstance()
        repo = Repository()
        updateCount()
    }


    fun updateCount(swipeRefreshLayout: SwipeRefreshLayout? = null){

        database.collection("ads")
            .whereEqualTo("user_id", uid)
            .whereEqualTo("type", "Oferta")
            .get()
            .addOnSuccessListener {
                countOffer.value = it.size()
                val query = database.collection("ads")
                    .whereEqualTo("user_id", uid)
                    .whereEqualTo("type", "Pedido")
                    .get()
                    .addOnSuccessListener {
                        countDemand.value = it.size()
                        if (swipeRefreshLayout != null) {
                            swipeRefreshLayout.isRefreshing = false
                        }
                    }
            }
    }



    val offer: LiveData<Int> = countOffer

    val demand: LiveData<Int> = countDemand
    private val _img = MutableLiveData<RequestBuilder<Drawable>>()
    private val _ads = MutableLiveData<ArrayList<Ad>>().apply {
        repo.getAdProfile(this, uid, _img)
    }

    var img: LiveData<RequestBuilder<Drawable>> = _img
    var ads: LiveData<ArrayList<Ad>> = _ads

    private val _status = MutableLiveData<Int>().apply {
        value = -1
    }

    fun resetStatus(){
        _status.value = -1
    }

    var status: LiveData<Int> = _status

    fun toTalk(getAd: Ad, user: User) {
        repo.processing(getAd, TALKING,_status)
    }

    fun uptadeAds(swipe: SwipeRefreshLayout? = null){
        repo.getAdProfile(_ads,uid, _img, swipe)
    }
    class Factory @Inject constructor(private var app: Application,private var email: String) :
        ViewModelProvider.Factory {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return if (modelClass.isAssignableFrom(ProfileViewModel::class.java!!)) {
                ProfileViewModel(application = app,uid = this.email) as T
            } else {
                throw IllegalArgumentException("ViewModel Not Found")
            }
        }

    }
}
