package org.destinosustentavel.dsmovel.vm

import android.app.Application
import android.content.ContentValues.TAG
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.RequestBuilder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import org.destinosustentavel.dsmovel.ABORT
import org.destinosustentavel.dsmovel.AVALIABLE
import org.destinosustentavel.dsmovel.DELETED
import org.destinosustentavel.dsmovel.model.Ad
import org.destinosustentavel.dsmovel.model.User
import org.destinosustentavel.dsmovel.tools.Repository
import kotlin.collections.ArrayList


class DoneConcludedViewModel(application: Application) : AndroidViewModel(application) {


    private var database: FirebaseFirestore
    private var auth: FirebaseAuth
    private var currentUser: FirebaseUser?
    private val repo: Repository


    init {
        database = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
        currentUser = auth.currentUser
        repo = Repository()
    }
    private val _user = MutableLiveData<ArrayList<User?>>().apply {
        value = ArrayList()
    }
    private val _img = MutableLiveData<ArrayList<RequestBuilder<Drawable>?>>().apply {
        value = ArrayList()
    }
    private val _ads = MutableLiveData<ArrayList<Ad>>().apply {
        repo.getDoneConcluded(this, _user, _img, getApplication())
    }

    val user: LiveData<ArrayList<User?>> = _user
    val img: LiveData<ArrayList<RequestBuilder<Drawable>?>> = _img
    var ads: LiveData<ArrayList<Ad>> = _ads



    fun newDone(ad: Ad){
        ad.user_id = currentUser?.email
        database.collection("ads")
            .add(ad)
            .addOnSuccessListener { documentReference ->
                ad.id = documentReference.id
                val list = ArrayList<Ad>()
                val users: ArrayList<User?> = ArrayList()
                val imgs: ArrayList<RequestBuilder<Drawable>?> = ArrayList()
                _ads.value?.let { list.addAll(it) }
                _user.value?.let { users.addAll(it) }
                _img.value?.let { imgs.addAll(it) }
                list.add(0, ad)
                users.add(0, null)
                imgs.add(0, null)
                _user.value = users
                _img.value = imgs
                _ads.value = list
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                newDone(ad)
                Log.w(TAG, "Error adding document", e)
            }

    }


    fun processing(position: Int, status: String){
        repo.processing(ads.value?.get(position)!!,status)
        ads.value?.get(position)?.status = status
        if(status== ABORT){
            ads.value?.get(position)?.status = AVALIABLE
            val imgs = _img.value
            database.collection("ads").document(ads.value?.get(position)?.id!!)
                .update("collect", null)
        }
    }

    fun delete(position: Int){
        database.collection("ads").document(ads.value?.get(position)?.id!!)
            .update("status", DELETED)
        _ads.value?.removeAt(position)
    }


    fun editDone(ad: Ad){
        database.collection("ads").document(ad.id!!)
            .set(ad)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference}")
            }
            .addOnFailureListener { e ->

                Log.w(TAG, "Error adding document", e)
            }

    }

    fun updateDone(swipe: SwipeRefreshLayout){
        repo.getDoneConcluded(_ads, _user, _img, getApplication(), swipe)
    }



}