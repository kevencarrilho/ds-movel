package org.destinosustentavel.dsmovel.fragments.setting.pj.legal

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import org.destinosustentavel.dsmovel.R
import org.destinosustentavel.dsmovel.databinding.FragmentAdressBinding
import org.destinosustentavel.dsmovel.model.Address

class LegalFragment : Fragment() {


    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentAdressBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_adress, container, false)
        val addressList: ArrayList<Address> = ArrayList()
        addressList.add(Address())
        addressList.add(Address())
        addressList.add(Address())
        val adress: AdapterLegal = AdapterLegal(addressList)
//        adress.list = it
        binding.recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
        binding.recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        binding.recyclerView.adapter = adress

        adress.onItemClick = {

//            adress.currentAd.value = it
//            fragmentManager?.let { it1 ->
//                DetailsAd().show(
//                    it1,
//                    ""
//                )
//            }
        }
//        binding.imageView.setImageResource(R.drawable.ic_legal)
//        binding.textView3.text = "Cadastrar Pessoa Jurídica"
//
//        binding.cardView.setOnClickListener {
////            (activity as MainActivity).addFragmantBackStack(CreateAdress())
//        }

        return binding.root
    }

}
