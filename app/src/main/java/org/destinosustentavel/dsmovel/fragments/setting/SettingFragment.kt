package org.destinosustentavel.dsmovel.fragments.setting

import android.content.Intent
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import org.destinosustentavel.dsmovel.Login
import org.destinosustentavel.dsmovel.R
import org.destinosustentavel.dsmovel.databinding.FragmentSettingBinding
import org.destinosustentavel.dsmovel.fragments.CreateUserFragment
import org.destinosustentavel.dsmovel.vm.SettingViewModel
import org.destinosustentavel.dsmovel.vm.UserViewModel

class SettingFragment : Fragment() {

    private lateinit var user: UserViewModel

    private val viewModel: SettingViewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this)[SettingViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        user =
            ViewModelProviders.of(requireActivity()).get(UserViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_setting, container, false)

        val binding = FragmentSettingBinding.bind(root)




        binding.optionsLayout.settingLogout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(context, Login::class.java)
            startActivity(intent)
            activity?.finish()
        }

        binding.optionsLayout.user.setOnClickListener {
            if (savedInstanceState == null) {
                activity?.supportFragmentManager?.let { it1 -> CreateUserFragment.newInstance(false)
                    .show(it1,"") }
            }
        }
        user.user.observe(viewLifecycleOwner, Observer{userIt->

            binding.profileLayout.name.setText(userIt.name)

                Glide
                    .with(this)
                    .load(userIt.photo)
                    .into(binding.profileLayout.profileImage)

            userIt.type?.let {
                binding.profileLayout.userType.text = userIt.type + " interessado em " + userIt.interest
            }

            userIt.latitude?.let {
                val geo = Geocoder(context)
                val addresses = geo.getFromLocation(userIt.latitude!!,userIt.longitude!!,1)
                if (addresses.size>0){
                    val address = addresses.get(0)
                    binding.profileLayout.city.text = address.getAddressLine(0)
                }
            }

        })

        binding.optionsLayout.address.setOnClickListener {
            Navigation.findNavController((activity as AppCompatActivity)
                , R.id.nav_host_fragment).navigate(R.id.to_create_address)
        }

        binding.optionsLayout.done.setOnClickListener {
            Navigation.findNavController((activity as AppCompatActivity),
                R.id.nav_host_fragment)
                .navigate(R.id.action_mainFragment_to_doneConcludedFragmant)
        }

        binding.optionsLayout.answer.setOnClickListener {
            Navigation.findNavController((activity as AppCompatActivity),
                R.id.nav_host_fragment)
                .navigate(R.id.action_mainFragment_to_answerConcludedFragment)
        }
        binding.optionsLayout.test.setOnClickListener {
            val url = "https://forms.gle/o1P8r13g7R4j55VP8"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
//            startActivity(Intent(context,QuestionsActivity::class.java))
        }
        viewModel.demand.observe(viewLifecycleOwner, Observer {
            binding.profileLayout.demand.text = it.toString()
        })

        viewModel.offer.observe(viewLifecycleOwner, Observer {
            binding.profileLayout.offer.text = it.toString()
        })

        binding.swipe.setOnRefreshListener {
            viewModel.updateCount(binding.swipe)
        }
        return root
    }


}