package org.destinosustentavel.dsmovel.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import org.destinosustentavel.dsmovel.AD_FLAG
import org.destinosustentavel.dsmovel.ARG_CREATE_AD
import org.destinosustentavel.dsmovel.R
import org.destinosustentavel.dsmovel.USER
import org.destinosustentavel.dsmovel.databinding.FragmentListBinding
import org.destinosustentavel.dsmovel.tools.AdapterNotifications
import org.destinosustentavel.dsmovel.vm.NotificationsViewModel

class NotificationsFragment : Fragment() {

    private lateinit var ad: AdapterNotifications
    private lateinit var notifyVM: NotificationsViewModel
    private lateinit var binding: FragmentListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.show()
        activity?.
            setTitle("Notificações")
        notifyVM =
            ViewModelProvider(requireActivity()).get(NotificationsViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_list, container, false)

        binding.text.setText("Você ainda não tem notificações.")
        binding.swipe.isEnabled = false
        notifyVM.ads.observe(viewLifecycleOwner, Observer {

            if(it.count()==0){
                binding.frame.visibility = View.VISIBLE
            } else {
                binding.frame.visibility = View.INVISIBLE
            }
            ad = AdapterNotifications(it, notifyVM.notification.value!!, notifyVM.user.value!!)

            binding.recyclerView.layoutManager = LinearLayoutManager(context)

            binding.recyclerView.adapter = ad

            ad.toGo = {ad, user ->
                activity?.findNavController(R.id.nav_host_fragment)
                    ?.navigate(R.id.action_mainFragment_to_adFragmant,Bundle().apply {
                    putParcelable(AD_FLAG, ad)
                    putParcelable(USER, user)
                })
            }

        })
        return binding.root
    }
}