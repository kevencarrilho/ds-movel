package org.destinosustentavel.dsmovel.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import org.destinosustentavel.dsmovel.*
import org.destinosustentavel.dsmovel.databinding.AdNotificationDetailsBinding
import org.destinosustentavel.dsmovel.model.Ad
import org.destinosustentavel.dsmovel.model.User
import org.destinosustentavel.dsmovel.vm.AdViewModel
import java.text.SimpleDateFormat


class AdFragmant: Fragment() {
    private lateinit var adViewModel: AdViewModel

    private lateinit var binding: AdNotificationDetailsBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.ad_notification, container,
            false)
        setHasOptionsMenu(true)
        binding.setLifecycleOwner(this)
        binding.swipe.isRefreshing = false
        val ad = arguments?.getParcelable<Ad>(AD_FLAG)
        val user = arguments?.getParcelable<User>(USER)
        val factory = AdViewModel.Factory(requireActivity().application,ad!!, user!!)
        adViewModel = ViewModelProvider(this,factory).get(AdViewModel::class.java)
        binding.swipe.setOnRefreshListener {
            adViewModel.updateDone(binding.swipe)
        }
        adViewModel.user.observe(viewLifecycleOwner, Observer {
            it?.let { setUser(binding,it) }?: run{
                setUserNull(binding)
            }

            binding.profile.setOnClickListener{
                Navigation.findNavController(
                    (activity as AppCompatActivity),
                    R.id.nav_host_fragment
                )
                    .navigate(R.id.action_adFragmant_to_profileFragment, Bundle().apply {
                        this.putParcelable(USER, user)
                    })
            }
        })

        adViewModel.ads.observe(viewLifecycleOwner, Observer {
            setData(binding,it)

            if(it.user_id==FirebaseAuth.getInstance().uid){
                done(binding,it)
                binding.btRight.setOnClickListener {
                    when(ad.status){
                        AVALIABLE -> {
                            activity?.findNavController(R.id.nav_host_fragment)?.navigate(R.id.to_create_ad,Bundle().apply {
                                putParcelable(ARG_CREATE_AD, ad)
                            })
                        }
                        else -> {
                            adViewModel.processing(AVALIABLE)
                        }

                    }
                }

                binding.btCenter.setOnClickListener {
                    when(ad.status){
                        AVALIABLE -> {
                            AlertDialog.Builder(requireContext())
                                .setTitle("Apagar anúncio")
                                .setMessage("Você está apagando o anúncio. Deseja continuar?")
                                .setPositiveButton("Sim", {it1, it2 ->
                                    adViewModel.delete()
                                })
                                .setNegativeButton("Não", null)
                                .create().show()
                        }
                        else ->  {
                            Navigation.findNavController(
                                (activity as AppCompatActivity),
                                R.id.nav_host_fragment
                            )
                                .navigate(R.id.action_adFragmant_to_messageFragment, Bundle().apply {
                                    this.putSerializable(USER, user)
                                })
                        }
                    }
                }
                binding.btLeft.setOnClickListener {
                    when(ad.status){
                        TALKING -> {
                            adViewModel.processing(TRANSPORT)
                        }
                        TRANSPORT -> {
                            adViewModel.processing(CONCLUDED)
                        }
                        else ->
                            activity?.findNavController(R.id.nav_host_fragment)
                                ?.navigate(R.id.to_create_ad,Bundle().apply {
                                    putParcelable(ARG_CREATE_AD, ad)
                                    putString(RENEW, RENEW)
                                })
                    }
                }
            } else{
                if(it.status== AVALIABLE) {
                    feed(binding)
                    binding.btRight.setOnClickListener {
                        when(ad.status){
                            AVALIABLE -> {
                                activity?.findNavController(R.id.nav_host_fragment)?.navigate(R.id.to_create_ad,Bundle().apply {
                                    putParcelable(ARG_CREATE_AD, ad)
                                })
                            }
                            else -> {
                                adViewModel.processing(AVALIABLE)
                            }

                        }
                    }

                    binding.btLeft.setOnClickListener {

                        AlertDialog.Builder(requireContext())
                            .setMessage("Você tem certeza que deseja atender o anúncio " +
                                    ad?.title + "?")
                            .setPositiveButton("Sim", {a,b ->
                                adViewModel.processing(TALKING)
                            })
                            .setNegativeButton("Não", null)
                            .create().show()
                    }
                }
                else {
                    answer(binding,ad)
                    binding.btRight.setOnClickListener {
                        AlertDialog.Builder(requireContext())
                            .setTitle("Desistir do anúncio")
                            .setMessage("Você tem certeza que deseja desistir deste anúncio?")
                            .setPositiveButton("Sim", {it1, it2 ->
                                adViewModel.processing(ABORT)
                            })
                            .setNegativeButton("Não", null)
                            .create().show()
                    }

                    binding.btLeft.setOnClickListener {
                        Navigation.findNavController(
                            (activity as AppCompatActivity),
                            R.id.nav_host_fragment
                        )
                            .navigate(R.id.action_adFragmant_to_messageFragment, Bundle().apply {
                                this.putSerializable(USER, user)
                            })
                    }

                }
            }

        })

        return binding.root
    }
    private fun setData(binding: AdNotificationDetailsBinding, ad: Ad){
        binding!!.title.text = ad.title
        binding.materialTypeText.text = ad.type + " de " + ad.material_type
        binding.amount.text = ad.amount.toString() + " " + ad.unity
        binding.createdAt.text =
            "Ativo desde " + SimpleDateFormat("dd/MM/yyyy").format(ad.created_at?.time) +
                    " ultima edição em " + SimpleDateFormat("dd/MM/yyyy").format(ad.edited_at?.time)
        binding.end.text = "Valido até " + SimpleDateFormat("dd/MM/yyyy").format(ad.end?.time)
        binding.description.text = ad.description

        binding.materialTypeImage.setImageResource(
            when (ad.material_type) {
                "Plastico" -> R.drawable.ic_plastic_recycler
                "Metal" -> R.drawable.ic_metal_recycler
                "Vidro" -> R.drawable.ic_glass_recycler
                "Papel" -> R.drawable.ic_paper_recycler
                "Eletrônico" -> R.drawable.ic_ewaste_recycler
                else -> R.drawable.ic_organic_recycler
            }
        )

        if(ad.sell) binding.sale.visibility = View.VISIBLE

        when(ad.status){
            AVALIABLE -> {
                binding.point.setBackgroundResource(R.drawable.point_blue)
                binding.divider1.setBackgroundResource(android.R.color.darker_gray)
                binding.point2.setBackgroundResource(R.drawable.point_gray)
                binding.divider2.setBackgroundResource(android.R.color.darker_gray)
                binding.point3.setBackgroundResource(R.drawable.point_gray)
                binding.divider3.setBackgroundResource(android.R.color.darker_gray)
                binding.point4.setBackgroundResource(R.drawable.point_gray)
                binding.status.setText(AVALIABLE)
            }
            TALKING -> {
                binding.point.setBackgroundResource(R.drawable.point_green)
                binding.divider1.setBackgroundResource(android.R.color.holo_green_dark)
                binding.point2.setBackgroundResource(R.drawable.point_blue)
                binding.divider2.setBackgroundResource(android.R.color.darker_gray)
                binding.point3.setBackgroundResource(R.drawable.point_gray)
                binding.divider3.setBackgroundResource(android.R.color.darker_gray)
                binding.point4.setBackgroundResource(R.drawable.point_gray)
                binding.status.setText(TALKING)
            }
            TRANSPORT ->{
                binding.point.setBackgroundResource(R.drawable.point_green)
                binding.divider1.setBackgroundResource(android.R.color.holo_green_dark)
                binding.point2.setBackgroundResource(R.drawable.point_green)
                binding.divider2.setBackgroundResource(android.R.color.holo_green_dark)
                binding.point3.setBackgroundResource(R.drawable.point_blue)
                binding.divider3.setBackgroundResource(android.R.color.darker_gray)
                binding.point4.setBackgroundResource(R.drawable.point_gray)
                binding.status.setText(TRANSPORT)
            }
            CONCLUDED -> {
                binding.point.setBackgroundResource(R.drawable.point_green)
                binding.divider1.setBackgroundResource(android.R.color.holo_green_dark)
                binding.point2.setBackgroundResource(R.drawable.point_green)
                binding.divider2.setBackgroundResource(android.R.color.holo_green_dark)
                binding.point3.setBackgroundResource(R.drawable.point_green)
                binding.divider3.setBackgroundResource(android.R.color.holo_green_dark)
                binding.point4.setBackgroundResource(R.drawable.point_green)
                binding.status.setText(CONCLUDED)
            }
        }
        binding.backgroung.setBackgroundColor(
            when (ad?.type) {
                "Oferta" -> binding.backgroung.resources.getColor(R.color.green)
                else -> binding.backgroung.resources.getColor(R.color.blue)
            }
        )

    }

    private fun setUser(binding: AdNotificationDetailsBinding, user: User){
        Glide.with(this).load(user.photo).into(binding!!.imgProfile)
        binding!!.imgProfile.visibility = View.VISIBLE
        binding.profileName.setText(user.name)
    }

    private fun setUserNull(binding: AdNotificationDetailsBinding){
        binding!!.imgProfile.setImageResource(R.drawable.ic_account)
        binding!!.imgProfile.visibility = View.VISIBLE
        binding.profileName.setText("Ninguém atendeu seu anúncio")
    }

    private fun feed(binding: AdNotificationDetailsBinding){
        binding!!.btRight.setImageResource(R.drawable.ic_mail)
        binding.btCenter.visibility = View.GONE
        binding.btLeft.setImageResource(R.drawable.ic_trash)
        binding.progress.visibility = View.GONE
    }

    private fun profile(binding: AdNotificationDetailsBinding){
        binding!!.btRight.setImageResource(R.drawable.ic_trash)
        binding.btCenter.visibility = View.GONE
        binding.btLeft.visibility = View.GONE
        binding.progress.visibility = View.GONE
        binding.toolbar.visibility = View.GONE
    }

    private fun done(binding: AdNotificationDetailsBinding, ad: Ad){

        when(ad.status){
            AVALIABLE -> {
                binding!!.btRight.setImageResource(R.drawable.ic_pencil)
                binding.btCenter.setImageResource(R.drawable.ic_rubbish)
                binding.btLeft.setImageResource(R.drawable.ic_next_disabled)
                binding.btLeft.isEnabled = false
            }
            TALKING -> {
                binding!!.btRight.setImageResource(R.drawable.ic_cancel)
                binding.btCenter.setImageResource(R.drawable.ic_mail)
                binding.btLeft.setImageResource(R.drawable.ic_next)
            }
            TRANSPORT ->{
                binding!!.btRight.setImageResource(R.drawable.ic_cancel)
                binding.btCenter.setImageResource(R.drawable.ic_mail)
                binding.btLeft.setImageResource(R.drawable.ic_tick)
            }
            CONCLUDED -> {
                binding!!.btRight.visibility = View.GONE
                binding.btCenter.visibility = View.GONE
                binding.btLeft.setImageResource(R.drawable.ic_refresh)
            }
        }

    }

    private fun answer(binding: AdNotificationDetailsBinding, ad: Ad){


        when(ad.status){
            CONCLUDED ->{
                binding!!.btRight.visibility = View.GONE
                binding.btCenter.visibility = View.GONE
                binding.btLeft.visibility = View.GONE

            }

            else->{
                binding!!.btRight.setImageResource(R.drawable.ic_cancel)
                binding.btCenter.visibility = View.GONE
                binding.btLeft.setImageResource(R.drawable.ic_mail)

            }
        }
    }
    companion object{

        fun newInstance(type: String) = AdFragmant()
            .apply {
                arguments = Bundle().apply {
                    putString(TYPE, type)
                }
            }
    }
}


//adViewModel.ads.observe(viewLifecycleOwner, Observer{
//    var viewType: Int
//    if(user.id.equals(FirebaseAuth.getInstance().uid)){
//        adapterAd = AdapterAd(
//            it,
//            adViewModel.user.value!!,
//            adViewModel.img.value!!,
//            AD_DONE
//        )
//        if(it.get(0).status.equals(DELETED)){
//            binding.swipe.isEnabled = false
//        }
//        adViewModel.img.observe(viewLifecycleOwner, Observer {
//            adapterAd.notifyItemChanged(0)
//        })
//        binding.recyclerView.layoutManager = LinearLayoutManager(context)
//        binding.recyclerView.adapter = adapterAd
//
//        binding.swipe.setOnRefreshListener {
//            adViewModel.updateDone(binding.swipe)
//        }
//
//        adapterAd.onbtRightClick = { i, ad, user ->
//
//            when(ad.status){
//                AVALIABLE -> {
//                    activity?.findNavController(R.id.nav_host_fragment)?.navigate(R.id.action_adFragmant_to_createAdress,Bundle().apply {
//                        putParcelable(ARG_CREATE_AD, ad)
//                    })
//                }
//                else -> {
//                    adViewModel.processing(i, ABORT)
//                    adapterAd.notifyItemChanged(i)
//                }
//
//            }
//
//        }
//
//        adapterAd.onbtLeftClick = { i, ad, user ->
//            when(ad.status){
//                TALKING -> {
//                    adViewModel.processing(i, TRANSPORT)
//                    adapterAd.notifyItemChanged(i)
//                }
//                TRANSPORT -> {
//                    adViewModel.processing(i, CONCLUDED)
//                    this.adapterAd.notifyItemChanged(i)
//                }
//                else ->
//                    activity?.findNavController(R.id.nav_host_fragment)
//                        ?.navigate(R.id.action_adFragmant_to_createAdress,Bundle().apply {
//                            putParcelable(ARG_CREATE_AD, ad)
//                            putString(RENEW, RENEW)
//                        })
//            }
//
//        }
//
//        adapterAd.onbtCenterClick = { i, ad, user ->
//            when(ad.status){
//                AVALIABLE -> {
//                    AlertDialog.Builder(requireContext())
//                        .setTitle("Apagar anúncio")
//                        .setMessage("Você está apagando o anúncio. Deseja continuar?")
//                        .setPositiveButton("Sim", {it1, it2 ->
//                            adViewModel.delete(i)
//                            this.adapterAd.notifyItemRemoved(i)
//                        })
//                        .setNegativeButton("Não", null)
//                        .create().show()
//                }
//                else ->  {
//                    Navigation.findNavController(
//                        (activity as AppCompatActivity),
//                        R.id.nav_host_fragment
//                    )
//                        .navigate(R.id.action_adFragmant_to_messageFragment, Bundle().apply {
//                            this.putSerializable(USER, user)
//                        })
//                }
//            }
//        }
//        adapterAd.onbtProfileClick = {user ->
//            Navigation.findNavController(
//                (activity as AppCompatActivity),
//                R.id.nav_host_fragment
//            )
//                .navigate(R.id.action_adFragmant_to_profileFragment, Bundle().apply {
//                    this.putParcelable(USER, user)
//                })
//        }
//    }
//    else
//    {
//        adapterAd = AdapterAd(
//            it,
//            adViewModel.user.value!!,
//            adViewModel.img.value!!,
//            AD_FEED
//        )
//        binding.recyclerView.layoutManager = LinearLayoutManager(context)
//        binding.recyclerView.adapter = adapterAd
//
//        adapterAd.onbtRightClick = { i, ad, user ->
//            Navigation.findNavController(
//                (activity as AppCompatActivity),
//                R.id.nav_host_fragment
//            )
//                .navigate(R.id.action_adFragmant_to_messageFragment, Bundle().apply {
//                    this.putSerializable(USER, user)
//                })
//        }
//
//        adapterAd.onbtLeftClick = {i, ad, user ->
//
//            androidx.appcompat.app.AlertDialog.Builder(requireContext())
//                .setMessage("Você tem certeza que deseja atender o anúncio " +
//                        ad?.title + "?")
//                .setPositiveButton("Sim", {a,b ->
//                    adViewModel.toTalk(ad!!)
//                })
//                .setNegativeButton("Não", null)
//                .create().show()
//        }
//
//        adapterAd.onbtProfileClick = {user ->
//            Navigation.findNavController(
//                (activity as AppCompatActivity),
//                R.id.nav_host_fragment
//            )
//                .navigate(R.id.action_adFragmant_to_profileFragment, Bundle().apply {
//                    this.putParcelable(USER, user)
//                })
//        }
//    }
//
//})