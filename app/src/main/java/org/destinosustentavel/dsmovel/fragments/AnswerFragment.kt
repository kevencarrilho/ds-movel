package org.destinosustentavel.dsmovel.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import org.destinosustentavel.dsmovel.AD_ANSWER
import org.destinosustentavel.dsmovel.R
import org.destinosustentavel.dsmovel.USER
import org.destinosustentavel.dsmovel.databinding.FragmentListBinding
import org.destinosustentavel.dsmovel.tools.AdapterAd
import org.destinosustentavel.dsmovel.vm.AnswerViewModel

class AnswerFragment : Fragment() {
    private lateinit var adapterAd: AdapterAd
    private val answerViewModel: AnswerViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[AnswerViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
    }
    private var database: FirebaseFirestore
    private var auth: FirebaseAuth
    private var currentUser: FirebaseUser?
    init {
        database = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
        currentUser = auth.currentUser
    }

    private lateinit var binding: FragmentListBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container,
            false)
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.swipe.setOnRefreshListener {
            answerViewModel.update(binding.swipe)
        }
        binding.setLifecycleOwner(this)
        binding.swipe.isRefreshing = false

        binding.text.setText("Nenhum anúncio disponível")

    }

    override fun onResume() {
        super.onResume()
        answerViewModel.ads.observe(this.viewLifecycleOwner, Observer{
            if(it.count()==0){
                binding.frame.visibility = View.VISIBLE
            } else {
                binding.frame.visibility = View.INVISIBLE
            }
            adapterAd = AdapterAd(
                it,
                answerViewModel.user.value!!,
                answerViewModel.img.value!!,
                AD_ANSWER
            )
            adapterAd.notifyItemInserted(0)
            binding.recyclerView.layoutManager = LinearLayoutManager(context)
            binding.recyclerView.adapter = adapterAd



            adapterAd.onbtRightClick = { i, ad, user ->
                AlertDialog.Builder(requireContext())
                    .setTitle("Desistir do anúncio")
                    .setMessage("Você tem certeza que deseja desistir deste anúncio?")
                    .setPositiveButton("Sim", {it1, it2 ->
                        answerViewModel.processing(i)
                        adapterAd.notifyItemRemoved(i)
                    })
                    .setNegativeButton("Não", null)
                    .create().show()
            }

            adapterAd.onbtLeftClick = { i, ad, user ->
                Navigation.findNavController(
                    (activity as AppCompatActivity),
                    R.id.nav_host_fragment
                )
                    .navigate(R.id.toMessage, Bundle().apply {
                        this.putSerializable(USER, user)
                    })
            }
            adapterAd.onbtProfileClick = {user ->
                Navigation.findNavController(
                    (activity as AppCompatActivity),
                    R.id.nav_host_fragment
                )
                    .navigate(R.id.action_mainFragment_to_profileFragment, Bundle().apply {
                        this.putParcelable(USER, user)
                    })
            }
        })
    }
}