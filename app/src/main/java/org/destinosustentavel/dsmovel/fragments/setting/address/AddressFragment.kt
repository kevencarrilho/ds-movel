package org.destinosustentavel.dsmovel.fragments.setting.address

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import org.destinosustentavel.dsmovel.R
import org.destinosustentavel.dsmovel.databinding.FragmentAdressBinding
import org.destinosustentavel.dsmovel.model.Address

class AddressFragment : Fragment() {


    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentAdressBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_adress, container, false)
        val addressList: ArrayList<Address> = ArrayList()
        addressList.add(Address())
        val address: AdapterAddress = AdapterAddress(addressList)
//        adress.list = it
        binding.recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
        binding.recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        binding.recyclerView.adapter = address

        address.onItemClick = {
            findNavController((activity as AppCompatActivity),R.id.nav_host_fragment).navigate(R.id.to_create_address)
//            (activity as MainActivity).addFragmantBackStack(CreateAdAddress())
        }


//        binding.imageView.setImageResource(R.drawable.ic_add_place)
//
//        binding.cardView.setOnClickListener {
//            (activity as MainActivity).addFragmantBackStack(CreateAdress())
//        }

        return binding.root
    }

}
