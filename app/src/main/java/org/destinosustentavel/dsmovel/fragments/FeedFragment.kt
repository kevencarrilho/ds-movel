package org.destinosustentavel.dsmovel.fragments

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import org.destinosustentavel.dsmovel.AD_FEED
import org.destinosustentavel.dsmovel.R
import org.destinosustentavel.dsmovel.USER
import org.destinosustentavel.dsmovel.databinding.FragmentListBinding
import org.destinosustentavel.dsmovel.model.Ad
import org.destinosustentavel.dsmovel.model.User
import org.destinosustentavel.dsmovel.tools.AdapterAd
import org.destinosustentavel.dsmovel.vm.FeedViewModel
import org.destinosustentavel.dsmovel.vm.UserViewModel
import kotlin.collections.ArrayList

class FeedFragment : Fragment() {

    private lateinit var adapterAd: AdapterAd

    private lateinit var viewGroup: ViewGroup
    private val feedViewModel: FeedViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[FeedViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
    }
    private val userViewModel: UserViewModel by
    lazy {
        activity?.run {
            ViewModelProvider(this).get(UserViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    private lateinit var binding: FragmentListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewGroup = container!!
        (activity as AppCompatActivity).supportActionBar?.show()
        activity?.
        setTitle("Anúncios")
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container,
            false)
        setHasOptionsMenu(true)

        binding.text.setText("Nenhum anúncio disponível")
        feedViewModel.ads?.observe(viewLifecycleOwner, Observer<ArrayList<Ad>> {
            if(it.count()==0){
                binding.frame.visibility = View.VISIBLE
            } else {
                binding.frame.visibility = View.INVISIBLE
            }

            adapterAd = AdapterAd(
                it,
                feedViewModel.user.value!!,
                feedViewModel.img.value!!,
                AD_FEED
            )
            binding.recyclerView.layoutManager = LinearLayoutManager(context)
            binding.recyclerView.adapter = adapterAd

            adapterAd.onbtRightClick = { i, ad, user ->
                findNavController((activity as AppCompatActivity), R.id.nav_host_fragment)
                    .navigate(R.id.toMessage, Bundle().apply {
                        this.putSerializable(USER, user)
                    })
            }

            adapterAd.onbtLeftClick = {i, ad, user ->

                AlertDialog.Builder(requireContext())
                    .setMessage("Você tem certeza que deseja atender o anúncio " +
                            ad?.title + "?")
                    .setPositiveButton("Sim", {a,b ->
                        feedViewModel.toTalk(ad!!)
                    })
                    .setNegativeButton("Não", null)
                    .create().show()
            }

            adapterAd.onbtProfileClick = {user ->
                findNavController((activity as AppCompatActivity), R.id.nav_host_fragment)
                    .navigate(R.id.action_mainFragment_to_profileFragment, Bundle().apply {
                        this.putParcelable(USER, user)
                    })
            }

        })
        binding.setLifecycleOwner(this)
        binding.swipe.isRefreshing = false
        binding.swipe.setOnRefreshListener {
            feedViewModel.uptadeAds(binding.swipe)
        }

        return binding.root
    }



    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_message, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.menu_message -> {
                findNavController(requireActivity(),R.id.nav_host_fragment)
                    ?.navigate(R.id.to_message)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}