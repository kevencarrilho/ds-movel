package org.destinosustentavel.dsmovel.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import org.destinosustentavel.dsmovel.*
import org.destinosustentavel.dsmovel.databinding.FragmentListBinding
import org.destinosustentavel.dsmovel.tools.AdapterAd
import org.destinosustentavel.dsmovel.vm.DoneViewModel


class DoneFragmant: Fragment() {
    private lateinit var adapterAd: AdapterAd
    private val doneViewModel: DoneViewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this)[DoneViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
    }

    private lateinit var binding: FragmentListBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container,
            false)
        setHasOptionsMenu(true)
        binding.setLifecycleOwner(this)
        binding.swipe.isRefreshing = false

        binding.text.setText("Você ainda não realizou nenhum anúncio")
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        doneViewModel.ads.observe(viewLifecycleOwner, Observer{
            if(it.count()==0){
                binding.frame.visibility = View.VISIBLE
            } else {
                binding.frame.visibility = View.INVISIBLE
            }
            adapterAd = AdapterAd(
                it,
                doneViewModel.user.value!!,
                doneViewModel.img.value!!,
                AD_DONE
            )
            binding.recyclerView.layoutManager = LinearLayoutManager(context)
            binding.recyclerView.adapter = adapterAd

            binding.swipe.setOnRefreshListener {
                doneViewModel.updateDone(binding.swipe)
            }

            adapterAd.onbtRightClick = { i, ad, user ->

                when(ad.status){
                    AVALIABLE -> {
                        activity?.findNavController(R.id.nav_host_fragment)?.navigate(R.id.to_create_ad,Bundle().apply {
                            putParcelable(ARG_CREATE_AD, ad)
                        })
                    }
                    else -> {
                        doneViewModel.processing(i, AVALIABLE)
                        adapterAd.notifyItemChanged(i)
                    }

                }

            }

            adapterAd.onbtLeftClick = { i, ad, user ->
                when(ad.status){
                    TALKING -> {
                        doneViewModel.processing(i, TRANSPORT)
                        adapterAd.notifyItemChanged(i)
                    }
                    TRANSPORT -> {
                        doneViewModel.processing(i, CONCLUDED)
                        this.adapterAd.notifyItemChanged(i)
                    }
                    else ->
                        activity?.findNavController(R.id.nav_host_fragment)
                            ?.navigate(R.id.to_create_ad,Bundle().apply {
                                putParcelable(ARG_CREATE_AD, ad)
                                putString(RENEW, RENEW)
                            })
                }

            }

            adapterAd.onbtCenterClick = { i, ad, user ->
                when(ad.status){
                    AVALIABLE -> {
                        AlertDialog.Builder(requireContext())
                            .setTitle("Apagar anúncio")
                            .setMessage("Você está apagando o anúncio. Deseja continuar?")
                            .setPositiveButton("Sim", {it1, it2 ->
                                doneViewModel.delete(i)
                                this.adapterAd.notifyItemRemoved(i)
                            })
                            .setNegativeButton("Não", null)
                            .create().show()
                    }
                    else ->  {
                        Navigation.findNavController(
                            (activity as AppCompatActivity),
                            R.id.nav_host_fragment
                        )
                            .navigate(R.id.toMessage, Bundle().apply {
                                this.putSerializable(USER, user)
                            })
                    }
                }
            }

            adapterAd.onbtProfileClick = {user ->
                Navigation.findNavController(
                    (activity as AppCompatActivity),
                    R.id.nav_host_fragment
                )
                    .navigate(R.id.action_mainFragment_to_profileFragment, Bundle().apply {
                        this.putParcelable(USER, user)
                    })
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_my_ads, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.menu_my_ads_add -> {

                activity?.findNavController(R.id.nav_host_fragment)
                    ?.navigate(R.id.to_create_ad,Bundle()
                        .apply {
                            putString(RENEW, RENEW)
                        })
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object{

        fun newInstance(type: String) = DoneFragmant()
            .apply {
                arguments = Bundle().apply {
                    putString(TYPE, type)
                }
            }
    }
}