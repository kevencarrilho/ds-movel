package org.destinosustentavel.dsmovel.fragments.setting.pj

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.destinosustentavel.dsmovel.R

class PjFragment : Fragment() {

    companion object {
        fun newInstance() = PjFragment()
    }

    private lateinit var viewModel: PjViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.legal_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PjViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
