package org.destinosustentavel.dsmovel.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import org.destinosustentavel.dsmovel.R
import org.destinosustentavel.dsmovel.databinding.FragmentListBinding
import org.destinosustentavel.dsmovel.model.Message
import org.destinosustentavel.dsmovel.model.User
import org.destinosustentavel.dsmovel.vm.LastMessageViewModel

class MessageListFragment : Fragment() {

    companion object {
        fun newInstance() =
            MessageListFragment()
    }

    private lateinit var adapter: ContactAdapter


    private lateinit var binding: FragmentListBinding

    private lateinit var viewModelLast: LastMessageViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_list, container, false)

        viewModelLast = ViewModelProvider(requireActivity()).get(LastMessageViewModel::class.java)
        binding.swipe.isEnabled = false
        adapter = ContactAdapter()
        binding.text.setText("Você ainda não possui conversas.")
        viewModelLast.user.observe(viewLifecycleOwner, Observer {
            if(it.count()==0){
                binding.frame.visibility = View.VISIBLE
            } else {
                binding.frame.visibility = View.INVISIBLE
            }
            viewModelLast.messagens.value?.let { it2 ->

                adapter.list = it2
                adapter.user = it
                binding.recyclerView.layoutManager = LinearLayoutManager(context)
                binding.recyclerView.adapter = adapter

                adapter.onClick = {last, user->
                    val args = Bundle()
                    args.putSerializable("user", user)
                    Navigation.findNavController((activity as AppCompatActivity), R.id.nav_host_fragment).navigate(R.id.open_chat, args)
                }
            }

        })

        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.show()
        activity?.
            setTitle("Mensagens")
    }



    private inner class ContactAdapter(

    ): RecyclerView.Adapter<ContactAdapter.ViewHolder>(){
        var list = ArrayList<Message>()
        var user = ArrayList<User>()
        var onClick: ((Message, User) -> Unit)? = null
        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
            val container: ConstraintLayout
            val name: TextView
            val msg: TextView
            val profileImg: ImageView
            init {
                container = itemView.findViewById(R.id.container)
                name = itemView.findViewById(R.id.name)
                msg = itemView.findViewById(R.id.msg)
                profileImg = itemView.findViewById(R.id.profile_image)
                container.setOnClickListener {
                    onClick?.invoke(list.get(adapterPosition), user.get(adapterPosition))
                }

            }

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_contact, parent, false))
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val lastMessage: Message = list.get(position)
            val user = user.get(position)
            Glide
                .with(holder.itemView)
                .load(user.photo)
                .into(holder.profileImg)

            holder.name.setText(user.name)
            holder.msg.setText(lastMessage.text)

        }
    }

}
