package org.destinosustentavel.dsmovel.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import org.destinosustentavel.dsmovel.ANSWER
import org.destinosustentavel.dsmovel.DONE
import org.destinosustentavel.dsmovel.ORGANIZATION

import org.destinosustentavel.dsmovel.R


/**
 * A simple [Fragment] subclass.
 * Use the [MyAdsManagerFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MyAdsManagerFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private lateinit var viewPager: ViewPager2

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as AppCompatActivity).supportActionBar?.show()
        activity?.
        setTitle("Meus Anúncios")
        return inflater.inflate(R.layout.fragment_my_ads, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewPager = view.findViewById(R.id.pager)
        val tabLayout: TabLayout = view.findViewById(R.id.tabLayout)
        viewPager.adapter = MyAdsPagerAdapter(this)

        TabLayoutMediator(tabLayout,viewPager){tab, position->
             when(position){
                 0 ->  tab.text = DONE
                 1 ->  tab.text = ANSWER
                 else ->  tab.text = ORGANIZATION
             }
        }.attach()

    }

    inner class MyAdsPagerAdapter(fragment: Fragment): FragmentStateAdapter(fragment) {
        override fun getItemCount(): Int {
            return 2
        }

        override fun createFragment(position: Int): Fragment {
            when(position){
                0 -> return DoneFragmant()
                1 -> return AnswerFragment()
                else -> return AnswerFragment()
            }
        }


    }
}

