package org.destinosustentavel.dsmovel.fragments

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.RadioButton
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.firebase.Timestamp
import org.destinosustentavel.dsmovel.ARG_CREATE_AD
import org.destinosustentavel.dsmovel.AVALIABLE
import org.destinosustentavel.dsmovel.R
import org.destinosustentavel.dsmovel.RENEW
import org.destinosustentavel.dsmovel.databinding.CreatedAdBinding
import org.destinosustentavel.dsmovel.model.Ad
import org.destinosustentavel.dsmovel.tools.LocationListener
import org.destinosustentavel.dsmovel.vm.DoneViewModel
import org.destinosustentavel.dsmovel.vm.UserViewModel
import java.util.*



class CreateADFragment : Fragment() {

    private val myAdsViewModel: DoneViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[DoneViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
    }
    private val user: UserViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[UserViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
    }
    private lateinit var content: CreatedAdBinding
    private var ad: Ad = Ad()
    private var new: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        content = DataBindingUtil.inflate(inflater, R.layout.activity_create_ad, container, false)
        val binding = content.contentCreateAds
        activity?.setTitle("Novo Anúncio")

        arguments?.let {args ->
            args.getParcelable<Ad>(ARG_CREATE_AD)?.let {
                ad = it
            }
            args.getString(RENEW)?.let{
                new = it
            }
        }

        context?.let {
            ArrayAdapter.createFromResource(
                it,
                R.array.material_type,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                binding.materialType.adapter = adapter
            }
            ArrayAdapter.createFromResource(
                it,
                R.array.unities,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                binding.unity.adapter = adapter
            }
        }

        when(user.user.value?.interest){
            "Coletar" ->{
                binding.radioGroup.check(R.id.radio_demand)
            }
            "Descartar" ->{
                binding.radioGroup.check(R.id.radio_offer)
            }
        }
        ad.id?.let {

            if(!new.equals(RENEW)){
                binding.radioGroup.isEnabled = false
                binding.radioOffer.isEnabled = false
                binding.radioDemand.isEnabled = false
            }


            when (ad.type) {
                "Oferta" -> binding.radioGroup.check(R.id.radio_offer)
                else -> binding.radioGroup.check(R.id.radio_demand)

            }


            binding.title.append(ad.title)
            binding.amount.append(ad.amount.toString())
            binding.description.append(ad.description)

            binding.sale.isChecked = ad.sell

            when(ad.material_type){
                "Papel"-> binding.materialType.setSelection(0)
                "Metal"-> binding.materialType.setSelection(1)
                "Plastico"-> binding.materialType.setSelection(2)
                "Vidro"-> binding.materialType.setSelection(3)
                "Eletrônico"-> binding.materialType.setSelection(4)
                else-> binding.materialType.setSelection(5)
            }

            when(ad.unity){
                "unidade(s)"-> binding.unity.setSelection(0)
                "g"-> binding.unity.setSelection(1)
                "kg"-> binding.unity.setSelection(2)
                "cm"-> binding.unity.setSelection(3)
                "m"-> binding.unity.setSelection(4)
                "cm2"-> binding.unity.setSelection(5)
                "m2"-> binding.unity.setSelection(6)
                "cm3"-> binding.unity.setSelection(7)
                else -> binding.unity.setSelection(8)
            }
            val year = Integer.valueOf(DateFormat.format("yyyy", ad.end).toString())
            val month = Integer.valueOf(DateFormat.format("MM", ad.end).toString()) - 1
            val day = Integer.valueOf(DateFormat.format("dd", ad.end).toString())
            binding.end.updateDate(year,month,day)
        }

        binding.address.setOnClickListener {
//            Navigation.findNavController((activity as AppCompatActivity), R.id.nav_host_fragment).navigate(R.id.to_create_address_ad)
            var locationManager =  activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.INTERNET),0)
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1,10f,
                LocationListener(requireContext()
                    , null, ad)
            )

        }
        binding.saveButton.setOnClickListener{
            binding.saveButton.isEnabled = false

            ad.latitude = user.user.value?.latitude!!
            ad.longitude = user.user.value?.longitude!!
            if(binding.title.text.toString().equals("")){
                binding.title.setError("Campo obrigatório")
                binding.title.requestFocus()
            } else if(binding.amount.text.toString().equals("")){
                binding.amount.setError("Campo obrigatório")
                binding.amount.requestFocus()
            } else{


                val radioButton = binding.root.findViewById<RadioButton>(
                    binding.radioGroup.checkedRadioButtonId
                )
                ad.type = radioButton.text.toString()
                ad.title = binding.title.text.toString()
                ad.material_type = binding.materialType.selectedItem.toString()
                ad.amount = binding.amount.text.toString().toDouble()
                ad.unity = binding.unity.selectedItem.toString()
                val calendar = Calendar.getInstance()
                calendar.set(
                    binding.end.year,
                    binding.end.month,
                    binding.end.dayOfMonth
                )

                ad.end = calendar.time
                ad.description = binding.description.text.toString()

                ad.sell = binding.sale.isChecked

                ad.edited_at = Timestamp.now().toDate()
                if (Date().after(ad.end)){
                    activity?.let {
                        val dialog: AlertDialog? = AlertDialog.Builder(it)?.setMessage(
                            "Você está criando um anúncio expirado, tem certeza que deseja continuar?"
                        )
                            ?.setTitle("Atenção!")
                            ?.setPositiveButton(
                                "Sim", { dialog, id ->
                                    if(new.equals(RENEW)){
                                        ad.created_at = Timestamp.now().toDate()
                                        ad.status = AVALIABLE
                                        myAdsViewModel.newDone(ad)
                                    } else {
                                        myAdsViewModel.editDone(ad)
                                    }
                                    activity?.findNavController(R.id.nav_host_fragment)
                                        ?.popBackStack()

                                })
                            ?.setNegativeButton("Não", null)?.create()

                        dialog?.show()
                    }
                }
                else{
                    if(new.equals(RENEW)){
                        ad.created_at = Timestamp.now().toDate()
                        ad.status = AVALIABLE
                        myAdsViewModel.newDone(ad)
                    } else {
                        myAdsViewModel.editDone(ad)
                    }
                    activity?.findNavController(R.id.nav_host_fragment)
                        ?.popBackStack()
                }
            }

            binding.saveButton.isEnabled = true
        }
        return content.root
    }


    companion object{
        @JvmStatic
        fun newInstance(ad: Ad? = null, renew: Boolean = false) =
            CreateADFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_CREATE_AD, ad)
                    putByte(RENEW, if (renew) 1 else 0)
                }
            }

    }
}
