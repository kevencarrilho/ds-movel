package org.destinosustentavel.dsmovel.fragments.setting.address

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import org.destinosustentavel.dsmovel.R
import org.destinosustentavel.dsmovel.vm.UserViewModel


class CreateAdress : Fragment(), GoogleMap.OnMyLocationButtonClickListener,
    GoogleMap.OnMyLocationClickListener,
    OnMapReadyCallback{

    private var i: Boolean = true
    private lateinit var mMap: GoogleMap

    private val user: UserViewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this)[UserViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
    }

    private lateinit var dia: AlertDialog
    lateinit var builder: AlertDialog.Builder
    private lateinit var locationManager: LocationManager;
    private val  MIN_TIME: Long = 400
    private val  MIN_DISTANCE: Float = 1000.toFloat()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_place, container, false)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mapFragment.view?.isClickable = true
        activity?.let {
            builder = AlertDialog.Builder(it)
            dia = builder.create()
        }

        return root
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == 0) {
            if (permissions.size == 1 &&
                permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                mMap.setMyLocationEnabled(true)
                mMap.setOnMyLocationButtonClickListener(this)
                mMap.setOnMyLocationClickListener(this)

            } else {
                // Permission was denied. Display an error message.
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.INTERNET),0)
            }
        }
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true)
            mMap.setOnMyLocationButtonClickListener(this)
            mMap.setOnMyLocationClickListener(this)
        } else {

            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.INTERNET),0)
        }

        // Add a marker in Sydney and move the camera
//        val latLong = LatLngBounds(LatLng(-7.36131,-73.9531207),LatLng(-7.36131,-34.8001557))
//        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLong,5))
    }

    override fun onMyLocationButtonClick(): Boolean {
        return false
    }


    override fun onMyLocationClick(p0: Location) {

        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        if (isConnected) {
            var address = getAddress(p0)
            builder?.setMessage(
                "Deseja que " + address?.getAddressLine(0) +
                        " seja salvo como seu endereço principal?"
            )
                ?.setTitle("Novo Endereço")


            dia = builder
                ?.setPositiveButton("Sim", { dialog, id ->

                    user.user.value?.latitude = p0.latitude
                    user.user.value?.longitude =  p0.longitude
                    user.set(user.user.value!!)
                    user.save(user.user.value!!)
                    Navigation.findNavController((activity as AppCompatActivity), R.id.nav_host_fragment).popBackStack()

                })
                ?.setNegativeButton("Não", {dialog, id ->
                    i = true
                })?.create()!!

            if(i){
                i = true
                dia?.show()
            }
            i = false
        } else {
            builder?.setMessage("É preciso ter conexão com a internet para cadastrar ou editar o endereço.")
                ?.setTitle("Falha")
            dia = builder
                ?.setNegativeButton("Ok", {dialog, id ->

                    Navigation.findNavController((activity as AppCompatActivity), R.id.nav_host_fragment).popBackStack()
                })?.create()!!
            if(i){
                i = true
                dia?.show()
            }
            i = false

        }
    }

    private fun getAddress(location: Location): Address?{
        val geo = Geocoder(context)
        val addresses = geo.getFromLocation(location.latitude,location.longitude,1)
        if (addresses.size>0){
            return  addresses.get(0)
        } else return  null
    }


}
