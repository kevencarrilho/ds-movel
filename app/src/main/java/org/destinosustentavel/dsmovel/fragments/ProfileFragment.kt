package org.destinosustentavel.dsmovel.fragments

import android.graphics.drawable.Drawable
import android.location.Geocoder
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import org.destinosustentavel.dsmovel.AD_PROFILE
import org.destinosustentavel.dsmovel.R
import org.destinosustentavel.dsmovel.USER
import org.destinosustentavel.dsmovel.databinding.FragmentProfileBinding
import org.destinosustentavel.dsmovel.model.User
import org.destinosustentavel.dsmovel.tools.AdapterAd
import org.destinosustentavel.dsmovel.vm.ProfileViewModel
import org.destinosustentavel.dsmovel.vm.UserViewModel

class ProfileFragment: Fragment() {

    private lateinit var adapterMyAds: AdapterAd
    private lateinit var userIt: User
    private lateinit var binding: FragmentProfileBinding
    private lateinit var viewModel: ProfileViewModel
    private val currentUser: UserViewModel by
    lazy {
        activity?.run {
            ViewModelProvider(this).get(UserViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setHasOptionsMenu(true)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        userIt = arguments?.get(USER) as User
         val factory = ProfileViewModel.Factory(requireActivity().application,userIt.id!!)
         viewModel = ViewModelProvider(this, factory).get(ProfileViewModel::class.java)
        binding.profileLayout.name.setText(userIt.name)
        Glide
            .with(this)
            .load(userIt.photo)
            .into(binding.profileLayout.profileImage)

        userIt.type?.let {
            binding.profileLayout.userType.text = userIt.type + " interessado em " + userIt.interest
        }

        binding.swipe.isRefreshing = false
        binding.swipe.setOnRefreshListener {
            viewModel.uptadeAds(binding.swipe)
            viewModel.updateCount(binding.swipe)
        }
        userIt.latitude?.let {
            val geo = Geocoder(context)
            val addresses = geo.getFromLocation(userIt.latitude!!,userIt.longitude!!,1)
            if (addresses.size>0){
                val address = addresses.get(0)
                binding.profileLayout.city.text = address.getAddressLine(0)
            }
            binding.text.setText("Este usuário não possui anuncios disponíveis para tratamento.")
        }
        viewModel.demand.observe(viewLifecycleOwner, Observer {
            binding.profileLayout.demand.text = it.toString()
        })

        viewModel.offer.observe(viewLifecycleOwner, Observer {
            binding.profileLayout.offer.text = it.toString()
        })

        viewModel.ads.observe(this.viewLifecycleOwner, Observer {
            if(it.count()==0){
                binding.frame.visibility = View.VISIBLE
            } else {
                binding.frame.visibility = View.INVISIBLE
            }
            var userList = ArrayList<User?>()
            userList. add(userIt)
            var img = ArrayList<RequestBuilder<Drawable>?>()
            img.add(viewModel.img.value)
            adapterMyAds = AdapterAd(
                it,
                userList,
                img,
                AD_PROFILE
            )
            binding.recyclerView.layoutManager = LinearLayoutManager(context)
            binding.recyclerView.adapter = adapterMyAds

            adapterMyAds.onbtRightClick = {i, ad, user ->
                AlertDialog.Builder(requireContext())
                    .setMessage(
                        "Você tem certeza que deseja atender o anúncio " +
                                ad.title + "?"
                    )
                    .setPositiveButton("Sim", { a, b ->
                        viewModel.toTalk(ad, currentUser.user.value!!)
                    })
                    .setNegativeButton("Não", null)
                    .create().show()
            }
        })

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_message, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.menu_message -> {
                Navigation.findNavController(
                    (activity as AppCompatActivity),
                    R.id.nav_host_fragment
                )
                    .navigate(R.id.action_profileFragment_to_messageFragment, Bundle().apply {
                        this.putParcelable(USER, userIt)
                    })
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}