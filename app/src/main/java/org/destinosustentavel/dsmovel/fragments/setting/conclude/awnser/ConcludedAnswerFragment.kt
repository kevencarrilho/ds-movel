package org.destinosustentavel.dsmovel.fragments.setting.conclude.awnser

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import org.destinosustentavel.dsmovel.*
import org.destinosustentavel.dsmovel.databinding.FragmentListBinding
import org.destinosustentavel.dsmovel.tools.AdapterAd
import org.destinosustentavel.dsmovel.vm.DoneViewModel

class ConcludedAnswerFragment : Fragment() {
    private lateinit var ad: AdapterAd
    private val myAdsViewModel: DoneViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[DoneViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
    }
    private var database: FirebaseFirestore
    private var auth: FirebaseAuth
    private var currentUser: FirebaseUser?
    init {
        database = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
        currentUser = auth.currentUser
    }

    private lateinit var binding: FragmentListBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container,
            false)
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.setLifecycleOwner(this)
        binding.swipe.isRefreshing = false


    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_message, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection

        return when (item.itemId) {
            R.id.menu_message -> {
                findNavController(requireActivity(),R.id.nav_host_fragment)?.navigate(R.id.to_message)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}