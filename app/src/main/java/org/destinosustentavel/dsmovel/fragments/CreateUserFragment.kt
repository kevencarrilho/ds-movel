package org.destinosustentavel.dsmovel.fragments

import android.Manifest
import androidx.appcompat.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import org.destinosustentavel.dsmovel.Login
import org.destinosustentavel.dsmovel.MainActivity
import org.destinosustentavel.dsmovel.NEW_CREATE_USER
import org.destinosustentavel.dsmovel.R
import org.destinosustentavel.dsmovel.databinding.UserFragmentBinding
import org.destinosustentavel.dsmovel.model.User
import org.destinosustentavel.dsmovel.tools.LocationListener
import org.destinosustentavel.dsmovel.vm.UserViewModel
import java.util.*


class CreateUserFragment: DialogFragment() {



    private val user: UserViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[UserViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
    }
    private var new = false
    private val userAux = User()

    private lateinit var content: UserFragmentBinding

    fun showDialog(manager: FragmentManager, tag: String?) {
        val ft = manager.beginTransaction()
        ft.add(this, tag)
        ft.commitAllowingStateLoss()
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        content  = DataBindingUtil.inflate(inflater,
            R.layout.user_fragment, container, false)

        arguments?.let {
            new = it.getBoolean(NEW_CREATE_USER)
        }
        content.email.isEnabled = false

        context?.let {
            ArrayAdapter.createFromResource(
                it,
                R.array.user_type,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                content.userType.adapter = adapter
            }
            ArrayAdapter.createFromResource(
                it,
                R.array.interest,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                content.interest.adapter = adapter
            }
        }

        if(new){
            var currentUser: FirebaseUser?
            currentUser = FirebaseAuth.getInstance().currentUser


            currentUser?.photoUrl?.let {
                Glide
                    .with(this@CreateUserFragment)
                    .load(it)
                    .into(content.profileImage)
                userAux.photo = it.toString()
            }
            content.phone.setText(currentUser?.phoneNumber)
            content.name.setText(currentUser?.displayName)
            content.email.setText(currentUser?.email)



            var locationManager =  activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.INTERNET),0)
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1,10f,
                LocationListener(
                    requireContext(),userAux))
        } else {

            Glide
                .with(this@CreateUserFragment)
                .load(user.user.value?.photo)
                .into(content.profileImage)

            content.phone.setText(user.user.value?.phone)
            content.name.setText(user.user.value?.name)
            content.email.setText(user.user.value?.email)
            userAux.latitude = user.user.value?.latitude
            userAux.longitude = user.user.value?.longitude
            new = false
            when(user.user.value?.type){
                "Colaborador"-> content.userType.setSelection(0)
                "Artesão"-> content.userType.setSelection(1)
                else-> content.userType.setSelection(2)
            }
            when(user.user.value?.interest){
                "Coletar"-> content.interest.setSelection(0)
                "Descartar"-> content.interest.setSelection(1)
                else-> content.interest.setSelection(2)
            }
        }






        content.save.setOnClickListener{

            if(userAux.latitude==null){

                AlertDialog.Builder(requireContext()).setTitle("Carregamento do endereço")
                    .setMessage("É preciso aguardar o GPS localizar sua posição " +
                            "para pegar o seu endereço, não se preocupe se não está no endereço" +
                            " correto, você pode mudar ele a qualquer momento indo até o seu perfil!")
                    .create().show()

            } else {
                userAux.email = content.email.text.toString()
                userAux.name = content.name.text.toString()
                userAux.phone = content.phone.text.toString()
                userAux.interest = content.interest.selectedItem.toString()
                userAux.type = content.userType.selectedItem.toString()
                userAux.created = Calendar.getInstance().getTime()
                user.set(userAux)
                user.save(userAux)

                if (new){
                    val intent = Intent(context, MainActivity::class.java)
                    val b = Bundle()
                    b.putParcelable("user", userAux)
                    intent.putExtras(b)
                    startActivity(intent)
                } else{
                    dialog?.cancel()
                }
            }
        }
        return content.root
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        if(new){
            FirebaseAuth.getInstance().signOut()
            Toast.makeText(context,"É necessária a conclusão do cadastro.", Toast.LENGTH_LONG).show()
            startActivity(Intent(context, Login::class.java))
            activity?.finish()
        } else {
            dialog.cancel()
        }
    }
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onDestroy() {
        super.onDestroy()
    }
    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BlankFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: Boolean) =
            CreateUserFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(NEW_CREATE_USER, param1)
                }
            }
    }
}