package org.destinosustentavel.dsmovel.fragments.setting.address

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import org.destinosustentavel.dsmovel.R
import org.destinosustentavel.dsmovel.model.Address

class AdapterAddress(var list: ArrayList<Address>?) : RecyclerView.Adapter<AdapterAddress.ViewHolder>() {


    var onItemClick: ((Address) -> Unit)? = null
    var position: Int = 0

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var cardView: CardView
        var imageView: ImageView
        var text: TextView
        init {
            cardView = itemView.findViewById(R.id.card_container)
            imageView = itemView.findViewById(R.id.imageView)
            text = itemView.findViewById(R.id.text)
            cardView.setOnClickListener{
                list?.get(adapterPosition)?.let { ad -> onItemClick?.invoke(ad) }
            }
        }
    }

    fun add(address: Address){
        list?.add(0, address)
        notifyItemChanged(0)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

            return ViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_add, parent, false))
    }

    override fun getItemCount(): Int {
        var count: Int = 0
        list?.let { count = it.count() }
        return count
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        this.position = position + 1
        if(position==0){
            holder.imageView.setImageResource(R.drawable.ic_add_place)
            holder.text.text = "Cadastrar"
        } else{
            holder.imageView.visibility = View.GONE
            holder.text.text = list?.get(position)?.cep
        }
    }
}