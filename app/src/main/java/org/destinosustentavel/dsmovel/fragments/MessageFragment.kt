package org.destinosustentavel.dsmovel.fragments

import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getSystemService
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import org.destinosustentavel.dsmovel.R
import org.destinosustentavel.dsmovel.USER
import org.destinosustentavel.dsmovel.databinding.FragmentMessageBinding
import org.destinosustentavel.dsmovel.model.Message
import org.destinosustentavel.dsmovel.model.User
import org.destinosustentavel.dsmovel.vm.MessageViewModel
import java.text.SimpleDateFormat


class MessageFragment : Fragment() {

    companion object {
        fun newInstance() =
            MessageFragment()
    }

    private lateinit var adapterMessage: AdapterMessage
    private lateinit var toUser: User
    private lateinit var binding: FragmentMessageBinding
    private lateinit var viewModel: MessageViewModel

    private val fromUser = FirebaseAuth.getInstance()
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate<FragmentMessageBinding>(
            inflater,
            R.layout.fragment_message,
            container,
            false
        )

        toUser = arguments?.getSerializable(USER) as User

        val factory = MessageViewModel.Factory(toUser.id!!)
        adapterMessage = AdapterMessage()
        viewModel = ViewModelProvider(this, factory)[MessageViewModel::class.java]
        binding.messageList.layoutManager = LinearLayoutManager(context,LinearLayoutManager.VERTICAL, true)
        binding.messageList.adapter = adapterMessage

        viewModel.messages.value?.let{
            adapterMessage.messages = it
            binding.messageList.smoothScrollToPosition(it.size)
        }
        viewModel.messagen.observe(viewLifecycleOwner, Observer {
            adapterMessage.messages.add(0,it)
            adapterMessage.notifyItemChanged(0)
            binding.messageList.smoothScrollToPosition(0)
            viewModel.addMessagem()

        })

        binding.send.setOnClickListener {
            if(!binding.text.text.isNullOrEmpty()){
                fromUser?.uid?.let { fromId ->
                    viewModel.send(
                        Message(fromId,toUser.id!!,binding.text.text.toString())
                    )}
                binding.text.setText(null)
            }
            val vibrator = getSystemService<Vibrator>(requireContext(),Vibrator::class.java)
            vibrator?.vibrate(VibrationEffect.createOneShot(50,255))
        }

        return binding.root

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as AppCompatActivity).title = toUser.name

        // TODO: Use the ViewModel
    }

    private inner class AdapterMessage(): RecyclerView.Adapter<AdapterMessage.MyMessage>() {
        inner class MyMessage(itemView: View): RecyclerView.ViewHolder(itemView) {
            val message: TextView
            val time: TextView
            init {
                message = itemView.findViewById(R.id.messagem)
                time = itemView.findViewById(R.id.time)
            }

        }

        var messages = ArrayList<Message>()

        override fun getItemViewType(position: Int): Int {
            var message = messages.get(position)
            if(message.fromId.equals(FirebaseAuth.getInstance().uid))
                return 0
            else return 1
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyMessage {
            return when(viewType){
                0 -> MyMessage(
                    LayoutInflater.from(parent.context).inflate(R.layout.layout_my_message,parent, false)
                )
                else -> MyMessage(
                    LayoutInflater.from(parent.context).inflate(R.layout.layout_your_message,parent, false)
                )
            }
        }

        override fun getItemCount(): Int {
            return messages.size
        }

        override fun onBindViewHolder(holder: MyMessage, position: Int) {
            var message = messages.get(position)
            holder.message.setText(message.text)

            holder.time.setText(SimpleDateFormat("dd-MM-yy\nHH:mm").format(message.timestamp))

        }
    }

}
