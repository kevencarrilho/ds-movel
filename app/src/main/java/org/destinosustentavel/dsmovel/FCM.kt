package org.destinosustentavel.dsmovel

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.Person
import androidx.core.app.RemoteInput
import androidx.core.graphics.drawable.IconCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.destinosustentavel.dsmovel.model.Ad
import org.destinosustentavel.dsmovel.model.Notification
import org.destinosustentavel.dsmovel.model.User


class FCM: FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        var database = FirebaseFirestore.getInstance()
        val type = remoteMessage.data.get("type")


        val intent = Intent(this,Login::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        FirebaseAuth.getInstance().uid?.let {

            database.collection("user").document(it)
                .get().addOnSuccessListener {
                    val myUser = it.toObject(User::class.java)
                    Log.i("TESTE FCM", type)
                    val channel = "MSG"
                    val notificationManager =
                        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        var channel = NotificationChannel(
                            channel, "My Notifications",
                            NotificationManager.IMPORTANCE_DEFAULT
                        )



                        channel.description = "Canal de notificação do DS Móvel"
                        channel.enableLights(true)
                        channel.lightColor = Color.RED
                        notificationManager.createNotificationChannel(channel)
                    }

                    when (type) {
                        "notification" -> {
                            var data = remoteMessage.data
                            database.collection("user")
                                .document(data.get("userId")!!)
                                .collection("notifications")
                                .document(data.get("notificationId")!!)
                                .get().addOnSuccessListener {
                                    var notification = it.toObject(Notification::class.java)
                                    database.collection("user")
                                        .document(notification?.from!!)
                                        .get().addOnSuccessListener {
                                            var user = it.toObject(User::class.java)
                                            database.collection("ads")
                                                .document(notification.adId!!)
                                                .get().addOnSuccessListener {
                                                    var ad = it.toObject(Ad::class.java)

                                                    var builder =
                                                        NotificationCompat.Builder(this, channel)

                                                    builder.setSmallIcon(R.drawable.ic_ads)
                                                    builder.setPriority(NotificationManager.IMPORTANCE_DEFAULT)

                                                    when (notification?.action) {
                                                        ABORT -> {
                                                            builder.setContentTitle("Usuário desistiu")
                                                            builder.setContentText(user?.name + " desistiu de atender o anúncio " + ad?.title)
                                                        }
                                                        TALKING -> {
                                                            builder.setContentTitle("Anúncio atendido")
                                                            builder.setContentText(user?.name + " atendeu o anúncio " + ad?.title)
                                                        }
                                                        TRANSPORT -> {
                                                            builder.setContentTitle("Disponível para transporte")
                                                            builder.setContentText(user?.name + " está esperando o tranporte do material no anúncio " + ad?.title)
                                                        }
                                                        CONCLUDED -> {
                                                            builder.setContentTitle("Anúncio concluído")
                                                            builder.setContentText(user?.name + " concluiu o anúncio " + ad?.title)
                                                        }
                                                    }

                                                    builder.setContentIntent(pendingIntent)
                                                    val img = Glide.with(this)
                                                        .asBitmap()
                                                        .load(user?.photo)
                                                        .circleCrop()
                                                        .into(object :
                                                            CustomTarget<Bitmap>(100, 100) {
                                                            override fun onLoadCleared(placeholder: Drawable?) {
                                                            }

                                                            override fun onResourceReady(
                                                                resource: Bitmap,
                                                                transition: Transition<in Bitmap>?
                                                            ) {

                                                                builder.setLargeIcon(resource)
                                                                notificationManager.notify(
                                                                    data.get(
                                                                        "notificationId"
                                                                    ).hashCode(), builder.build()
                                                                )

                                                            }

                                                        })


                                                }
                                        }

                                }

                        }
                        "messagen" -> {
                            val msg = remoteMessage.data
                            database.collection("user").document(msg.get("fromId")!!)
                                .get().addOnSuccessListener {

                                    val person = Person.Builder()
                                    person.setName(msg.get("name"))
                                    person.setImportant(true)

                                    val img = Glide.with(this)
                                        .asBitmap()
                                        .load(msg.get("photo"))
                                        .circleCrop()
                                        .into(object : CustomTarget<Bitmap>(100, 100) {
                                            override fun onLoadCleared(placeholder: Drawable?) {
                                            }

                                            override fun onResourceReady(
                                                resource: Bitmap,
                                                transition: Transition<in Bitmap>?
                                            ) {

                                                person.setIcon(IconCompat.createWithBitmap(resource))
                                                var remoteInput: RemoteInput =
                                                    RemoteInput.Builder("Responder").run {
                                                        setLabel("Responder")
                                                        build()
                                                    }

                                                val style =
                                                    NotificationCompat.MessagingStyle(person.build())
                                                style.addMessage(
                                                    msg.get("text"),
                                                    Timestamp.now().toDate().time,
                                                    person.build()
                                                )
                                                val msgNotification =
                                                    NotificationCompat.Builder(this@FCM, channel)
                                                msgNotification.setStyle(style)
                                                    .setSmallIcon(R.drawable.ic_send)
                                                    .setPriority(NotificationManager.IMPORTANCE_DEFAULT)
                                                    .setContentIntent(pendingIntent)
                                                    .build()

                                                notificationManager.notify(
                                                    msg.get("msgId").hashCode(),
                                                    msgNotification.build()
                                                )
                                            }

                                        })
                                }
                        }
                        "test" -> {
                            var builder =
                                NotificationCompat.Builder(this, channel)
                            builder.setSmallIcon(R.drawable.ic_ads)
                                .setPriority(NotificationManager.IMPORTANCE_DEFAULT)
                                .setContentTitle("Avaliação do aplicativo")
                                .setContentText("Por favor, participe da nossa pesquisa sobre o aplicativo móvel, toque aqui e responda nosso questionário.")
                            val url = "https://forms.gle/o1P8r13g7R4j55VP8"
                            val i = Intent(Intent.ACTION_VIEW)
                            i.data = Uri.parse(url)
                            val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, i, 0)
                            builder.setContentIntent(pendingIntent)
                            notificationManager.notify(
                                0,
                                builder.build()
                            )
                        }
                    }
                }
        }
    }
}