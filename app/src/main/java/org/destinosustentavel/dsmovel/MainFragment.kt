package org.destinosustentavel.dsmovel

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.destinosustentavel.dsmovel.vm.UserViewModel

class MainFragment : Fragment() {


    private var navView: BottomNavigationView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val root = inflater.inflate(R.layout.fragment_main, container, false)
        navView =
            root.findViewById(R.id.nav_view)

        val fragment: View = root.findViewById(R.id.main_fragment)
        val navController = fragment.findNavController()
        navView!!.setupWithNavController(navController)
        return root
    }

}
