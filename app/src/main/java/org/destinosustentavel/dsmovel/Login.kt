package org.destinosustentavel.dsmovel

import android.app.NotificationManager
import android.content.ContentValues.TAG
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.iid.FirebaseInstanceId
import org.destinosustentavel.dsmovel.databinding.ActivityLoginBinding
import org.destinosustentavel.dsmovel.databinding.ContentLoginBinding
import org.destinosustentavel.dsmovel.model.User
import org.destinosustentavel.dsmovel.fragments.CreateUserFragment


class Login() : AppCompatActivity(){

    private lateinit var auth: FirebaseAuth
    private lateinit var loginItens: ContentLoginBinding
    private lateinit var gsClient: GoogleSignInClient

    private var currentUser: FirebaseUser? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loginItens = (DataBindingUtil.setContentView(this, R.layout.activity_login)
                as ActivityLoginBinding).login

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.cancelAll()
        supportActionBar?.hide()
        buttonHide()
        auth = FirebaseAuth.getInstance()
        currentUser = FirebaseAuth.getInstance().currentUser
        currentUser?.let {
            logar()
        }?: run{buttonShow()}


        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .requestServerAuthCode(getString(R.string.server_client_id))
            .requestIdToken(getString(R.string.server_client_id))
            .build()

        gsClient = GoogleSignIn.getClient(this, gso)
        gsClient.signOut()



        loginItens.button.setOnClickListener {

            if(loginItens.username .text.isNullOrEmpty()){
                loginItens.username.error = "Esse campo é obrigatório"
                loginItens.username.requestFocus()
            } else if (loginItens.password.text.isNullOrEmpty()){
                loginItens.password.error = "Esse campo é obrigatório"
                loginItens.password.requestFocus()
            } else {
                buttonHide()
                auth.signInWithEmailAndPassword(
                    loginItens.username.text.toString(),
                    loginItens.password.text.toString()
                ).addOnCompleteListener { task ->
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success")
                        logar()
                    } else {
                        buttonShow()
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                        Toast.makeText(
                            this, "Falha no login, tente novamente.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }
        

        var cadastre_se = findViewById<Button>(R.id.create_user)
        cadastre_se.setOnClickListener {

            val dialog = AlertDialog.Builder(this)
                .setTitle("Novo Usuario")
                .setView(R.layout.user_create)
                .setPositiveButton("Salvar", null)
                .show()



            dialog.getButton(DialogInterface.BUTTON_POSITIVE)
                .setOnClickListener {
                    val email: EditText? = dialog.findViewById(R.id.email)
                    val password: EditText? = dialog.findViewById(R.id.password)
                    if(email?.text.isNullOrEmpty()){
                        email?.error = "Esse campo é obrigatório"
                        email?.requestFocus()
                    } else if (password?.text.isNullOrEmpty()){
                        password?.error = "Esse campo é obrigatório"
                        password?.requestFocus()
                    } else {
                        auth.createUserWithEmailAndPassword(
                            email?.text.toString(),
                            password?.text.toString()
                        )
                            .addOnCompleteListener { task ->


                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    logar()
                                    dialog.hide()
                                } else {
                                    // If sign in fails, display a message to the user.

                                    Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                    Toast.makeText(
                                        this, task.exception?.localizedMessage,
                                        Toast.LENGTH_LONG
                                    ).show();


                                }
                            }
                    }
                }

            dialog.show()

        }




        loginItens.signInButton.setSize(SignInButton.SIZE_WIDE)
        loginItens.signInButton.setOnClickListener{
            var intent = gsClient.signInIntent

            startActivityForResult(intent, 0)
            buttonHide()
        }



    }

    private fun logar() {
        FirebaseFirestore.getInstance().collection("user").
        document(auth.uid.toString())
            .get().addOnSuccessListener {
                if (it.getDate("created")==null){
                    supportFragmentManager.beginTransaction().commitAllowingStateLoss()
                    val create = CreateUserFragment.newInstance(true)

                    create.showDialog(supportFragmentManager,"")
                }else{
                    val userAux: User = it.toObject(User::class.java)!!
                    val intent = Intent(this@Login, MainActivity::class.java)
                    val b = Bundle()
                    b.putSerializable("user", userAux)
                    intent.putExtras(b)
                    startActivity(intent)
                    finish()
                }

            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 0){

            var task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSingInResult(task)
        }
    }

    private fun handleSingInResult(task: Task<GoogleSignInAccount>){
        try {
            val account = task.getResult(ApiException::class.java)
            account?.let{
                login(it)
            }
        } catch (e: ApiException){
            Log.w("", "signInResult:failed code=" + e.getStatusCode())
            buttonShow()
            if(e.statusCode==7){
                Toast.makeText(this,"Internet indisponível", Toast.LENGTH_LONG).show()
            }
        }
    }


    private fun login(acct: GoogleSignInAccount){

        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.id!!)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)

        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    logar()

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    Snackbar.make(loginItens.logo,"Authentication Failed.", Snackbar.LENGTH_SHORT).show()
                    buttonShow()
                }

                // ...
            }

    }

    private fun buttonHide(){

        loginItens.progressBar.visibility = View.VISIBLE
        loginItens.groupLogin.visibility = View.INVISIBLE
    }

    private fun buttonShow(){
        loginItens.progressBar.visibility = View.GONE
        loginItens.groupLogin.visibility = View.VISIBLE
    }

}


