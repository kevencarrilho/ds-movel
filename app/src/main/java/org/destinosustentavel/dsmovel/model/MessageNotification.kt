package org.destinosustentavel.dsmovel.model

import com.google.firebase.Timestamp
import java.util.*

data class MessageNotification (
    val fromId: String? = null,
    val toId: String? = null,
    var text: String? = null,
    var name: String? = null
)