package org.destinosustentavel.dsmovel.model

import java.util.*

data class Notification(
    var from: String? = null,
    var adId: String? = null,
    var action: String? = null,
    var created: Date? = null,
    var viewed: Boolean = false
)