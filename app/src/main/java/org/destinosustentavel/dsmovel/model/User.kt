package org.destinosustentavel.dsmovel.model

import android.location.Location
import android.net.Uri
import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.GeoPoint
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

data class User (
    var name:  String? = null,
    var email:  String? = null,
    var phone: String? = null,
    var photo: String? = "https://firebasestorage.googleapis.com/v0/b/ds-movel-15d76.appspot.com/o/boy.png?alt=media&token=9c6bc168-3eee-4c0d-9e86-73e1a6130bf3",
    var type: String? = null,
    var interest: String? = null,
    var latitude: Double? = null,
    var longitude: Double? = null,
    var evaluation: ArrayList<Double>? = null,
    var created: Date? = null,
    var token: ArrayList<String> = ArrayList(),
    var id: String? =null,
    var online: Boolean = false
): Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readArrayList(Double::class.java.classLoader) as? ArrayList<Double>,
        parcel.readValue(Date::class.java.classLoader) as Date?,
        parcel.readArrayList(String::class.java.classLoader) as ArrayList<String>,
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(email)
        parcel.writeString(phone)
        parcel.writeString(photo)
        parcel.writeString(type)
        parcel.writeString(interest)
        parcel.writeValue(latitude)
        parcel.writeValue(longitude)
        parcel.writeDoubleArray(evaluation?.toDoubleArray())
        parcel.writeValue(created)
        parcel.writeStringArray(arrayOf())
        parcel.writeString(id)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}