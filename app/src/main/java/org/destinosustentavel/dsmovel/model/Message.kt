package org.destinosustentavel.dsmovel.model

import com.google.firebase.Timestamp
import java.util.*


data class Message (
    var fromId: String? = null,
    var toId: String? = null,
    var text: String? = null,
    var timestamp: Date = Timestamp.now().toDate()
)