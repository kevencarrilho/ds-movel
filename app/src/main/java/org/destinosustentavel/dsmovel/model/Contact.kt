package org.destinosustentavel.dsmovel.model

import java.sql.Timestamp
import java.util.*

data class Contact (
    var name: String = "",
    var text: String = "",
    var photo: String = "",
    var timestamp: Date
)