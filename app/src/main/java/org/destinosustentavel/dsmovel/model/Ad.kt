package org.destinosustentavel.dsmovel.model

import android.os.Parcel
import android.os.Parcelable
import org.destinosustentavel.dsmovel.AVALIABLE
import java.util.*

data class Ad(
    var id: String? = null,
    var type: String? = null,
    var user_id: String? = null,
    var material_type: String? = null,
    var title: String? = null,
    var amount: Double? = null,
    var end: Date? = null,
    var unity: String? = null,
    var description: String? = null,
    var created_at: Date? = null,
    var edited_at: Date? = null,
    var status: String? = AVALIABLE,
    var latitude: Double? = null,
    var longitude: Double? = null,
    var pj: Boolean = false,
    var collect: String? = null,
    var sell: Boolean = false
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Date::class.java.classLoader) as Date?,
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Date::class.java.classLoader) as Date?,
        parcel.readValue(Date::class.java.classLoader) as Date?,
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readByte() != 0.toByte(),
                parcel.readString(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(type)
        parcel.writeString(user_id)
        parcel.writeString(material_type)
        parcel.writeString(title)
        parcel.writeValue(amount)
        parcel.writeString(unity)
        parcel.writeString(description)
        parcel.writeString(status)
        parcel.writeValue(latitude)
        parcel.writeValue(longitude)
        parcel.writeByte(if (pj) 1 else 0)
        parcel.writeValue(created_at)
        parcel.writeValue(edited_at)
        parcel.writeValue(end)
        parcel.writeString(title)
        parcel.writeByte(if (sell) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Ad> {
        override fun createFromParcel(parcel: Parcel): Ad {
            return Ad(parcel)
        }

        override fun newArray(size: Int): Array<Ad?> {
            return arrayOfNulls(size)
        }
    }
}

