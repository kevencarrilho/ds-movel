package org.destinosustentavel.dsmovel.model

import android.location.Location
import java.io.Serializable

data class Address (
    var cep: String? = null,
    var location: Location? = null
): Serializable